import 'package:flame/util.dart';
import 'package:flutter/cupertino.dart';
import 'package:hello/classes/boru.dart';
import 'package:hello/classes/box2dworld.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  Util flameUtil = Util();
  flameUtil.setLandscapeLeftOnly();
  flameUtil.fullScreen();
  Box2dWorld box = Box2dWorld();
  Boru game = Boru(box);
  runApp(game.widget);
}
