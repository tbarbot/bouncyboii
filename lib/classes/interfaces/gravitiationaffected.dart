import 'package:box2d_flame/box2d.dart';

abstract class GravitationAffected {
  bool isGravityReversed;
  int gravitationCount;
  List<Vector2> currentRepulsionSource = List<Vector2>();

  void reverseGravitation(double t) {}
  void setGravityReversed(int flag) {}
  void setRepulsion(Vector2 r) {}
  void removeRepulsion(Vector2 r) {}
}
