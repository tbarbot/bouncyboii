import 'dart:ui';

import 'package:flame/box2d/viewport.dart';
import 'package:flame/components/parallax_component.dart';
import 'package:box2d_flame/box2d.dart';

class ParallaxBg extends ParallaxComponent {
  Viewport viewport;
  Vector2 oldCenter;
  bool shouldUpdate = true;
  bool shouldDestroy = false;

  ParallaxBg(List<ParallaxImage> images, this.viewport,
      {baseSpeed = Offset.zero, layerDelta = Offset.zero})
      : super(images, baseSpeed: baseSpeed, layerDelta: layerDelta);

  @override
  void update(double t) {
    if (shouldUpdate) {
      if (oldCenter == null) {
        oldCenter = viewport.center;
      }
      double layerDeltaX = (viewport.center.x - oldCenter.x) * 10;
      double layerDeltaY = (oldCenter.y - viewport.center.y) * 10;
      layerDelta = Offset(layerDeltaX, layerDeltaY);
      oldCenter = Vector2(viewport.center.x, viewport.center.y);
    } else {
      layerDelta = Offset.zero;
    }
    super.update(t);
  }

  @override
  int priority() {
    return 1;
  }

  @override
  bool destroy() {
    return shouldDestroy;
  }
}
