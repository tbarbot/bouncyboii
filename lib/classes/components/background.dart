import 'dart:ui';

import 'package:flame/components/component.dart';
import 'package:hello/classes/utils/palette.dart';

class BackGroundComponent extends PositionComponent {
  Size screenSize = Size(0, 0);
  Color paint = Palette().curentPalette.quinary;

  BackGroundComponent() {
    x = 0;
    y = 0;
  }

  @override
  void render(Canvas c) {
    Rect bg = Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
    c.drawRect(bg, Paint()..color = paint);
  }

  @override
  void resize(Size size) {
    screenSize = size;
    width = screenSize.width;
    height = screenSize.height;
    super.resize(size);
  }

  @override
  void update(double t) {
    // TODO: implement update
  }
}
