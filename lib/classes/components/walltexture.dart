import 'dart:ui' as ui;

import 'package:flame/components/component.dart';
import 'package:flame/flame.dart';
import 'package:flutter/cupertino.dart';

class WallTextureComponent extends PositionComponent {
  Size screenSize = Size(0, 0);
  ui.Image image;
  bool isLoading = true;

  WallTextureComponent() {
    x = 0;
    y = 0;
    _loadImages();
  }

  void _loadImages() {
    Flame.images.load("text.png").then((img) {
      image = img;
      isLoading = false;
    });
  }

  @override
  void render(Canvas c) {
    if (!isLoading) {
      Rect bg = Rect.fromLTWH(0, 0, screenSize.width, screenSize.height);
      paintImage(
        canvas: c,
        image: image,
        rect: bg,
        fit: BoxFit.none,
        repeat: ImageRepeat.repeat,
      );
    }
    c.saveLayer(null, Paint());
    //c.drawRect(bg, Paint()..color = paint);
  }

  @override
  void resize(Size size) {
    screenSize = size;
    width = screenSize.width;
    height = screenSize.height;
    super.resize(size);
  }

  @override
  void update(double t) {
    // TODO: implement update
  }

  @override
  int priority() {
    return 20;
  }
}
