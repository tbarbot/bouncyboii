class StorageLevel {
  bool unlocked;
  int best;
  StorageLevel(this.unlocked, this.best);

  Map<String, dynamic> toJson() => {'unlocked': unlocked, 'best': best};

  static StorageLevel fromMap(Map<String, dynamic> map) {
    return StorageLevel(map['done'], map['best']);
  }
}
