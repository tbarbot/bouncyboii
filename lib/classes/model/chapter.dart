import 'package:hello/classes/model/level.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:yaml/yaml.dart';

class Chapter {
  int id;
  String name;
  List<Level> levels = [];
  Map levelMap;
  bool levelResolved = false;

  Chapter({this.id, this.name}) {
    populateLevels().then((map) {
      levelMap = map.value['levels'];
      levelResolved = true;
    });
  }

  Future<YamlMap> populateLevels() {
    return rootBundle
        .loadString('assets/chapters/$id.yml')
        .then((jsonStr) => loadYaml(jsonStr));
  }
}
