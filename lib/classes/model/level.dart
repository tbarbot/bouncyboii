import 'package:flame/box2d/box2d_component.dart';
import 'package:flutter/cupertino.dart';
import 'package:hello/classes/bodies/finish.dart';
import 'package:hello/classes/bodies/movingsaw.dart';
import 'package:hello/classes/bodies/nocorrectionzone.dart';
import 'package:hello/classes/bodies/pivotlaser.dart';
import 'package:hello/classes/bodies/repulser.dart';
import 'package:hello/classes/bodies/reversegravityzone.dart';
import 'package:hello/classes/bodies/saw.dart';
import 'package:hello/classes/bodies/sawpath.dart';
import 'package:hello/classes/bodies/throwingsaw.dart';
import 'package:hello/classes/bodies/wall.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:yaml/yaml.dart';

class Level {
  List<BodyComponent> bodies = List();
  int id;
  Offset spawn = Offset(0, 0);
  int par;
  String name;

  Level(this.id, Map bodyMap, Box2dWorld world) {
    loadLevel(world, bodyMap);
  }

  loadLevel(Box2dWorld world, Map bodyMap) {
    spawn = Offset(
        bodyMap['spawn']['x'].toDouble(), bodyMap['spawn']['y'].toDouble());
    name = bodyMap['name'];
    par = bodyMap['par'];
    bodyMap['bodies']
        .forEach((type, bodyMap2) => loadBody(world, type, bodyMap2));
  }

  loadBody(Box2dWorld world, String type, YamlList bodyMap) {
    switch (type) {
      case 'reversegravityzone':
        bodyMap.forEach((body) {
          ReverseGravityZone s = ReverseGravityZone.fromMap(world, body);
          bodies.add(s);
        });
        break;
      case 'wall':
        bodyMap.forEach((body) {
          Wall w = Wall.fromMap(world, body);
          bodies.add(w);
        });
        break;
      case 'finish':
        bodyMap.forEach((body) {
          Finish f = Finish(world, body['x'].toDouble(), body['y'].toDouble());
          bodies.add(f);
        });
        break;
      case 'saw':
        bodyMap.forEach((body) {
          Saw s = Saw.fromMap(world, body);
          bodies.add(s);
        });
        break;
      case 'movingsaw':
        bodyMap.forEach((body) {
          MovingSaw s = MovingSaw.fromMap(world, body);
          bodies.add(s);
          bodies.add(s.msl);
        });
        break;
      case 'throwingsaw':
        bodyMap.forEach((body) {
          SawThrower s = SawThrower.fromMap(world, body);
          bodies.add(s);
        });
        break;
      case 'nocorrection':
        bodyMap.forEach((body) {
          NoCorrectionZone nc = NoCorrectionZone.fromMap(world, body);
          bodies.add(nc);
        });
        break;
      case 'pivotlaser':
        bodyMap.forEach((body) {
          PivotLaser p = PivotLaser.fromMap(world, body);
          bodies.add(p);
          bodies.add(p.laser);
        });
        break;
      case 'sawpath':
        bodyMap.forEach((body) {
          SawPath sp = SawPath.fromMap(world, body);
          bodies.add(sp);
        });
        break;
      case 'repulser':
        bodyMap.forEach((body) {
          Repulser r = Repulser.fromMap(world, body);
          bodies.add(r);
        });
        break;
      default:
    }
  }
}
