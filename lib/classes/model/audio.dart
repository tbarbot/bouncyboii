class Audio {
  String file;
  double volume = 1.0;
  bool loop = false;

  Audio(this.file, this.loop, this.volume);
}
