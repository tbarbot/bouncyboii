import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/contact_callbacks.dart';
import 'package:hello/classes/bodies/ball.dart';
import 'package:hello/classes/bodies/wall.dart';

class BallWallContactCallback extends ContactCallback<Ball, Wall> {
  @override
  void begin(Ball ball, Wall w, Contact contact) {
    ball.collide();
  }

  @override
  void end(Ball ball, Wall w, Contact contact) {}
}
