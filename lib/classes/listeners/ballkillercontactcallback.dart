import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/contact_callbacks.dart';
import 'package:hello/classes/bodies/ball.dart';
import 'package:hello/classes/interfaces/killerbody.dart';

class BallKillerContactCallback extends ContactCallback<Ball, KillerBody> {
  @override
  void begin(Ball ball, KillerBody kb, Contact contact) {
    ball.kill();
  }

  @override
  void end(Ball ball, KillerBody kb, Contact contact) {}
}
