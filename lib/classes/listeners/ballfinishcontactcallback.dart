import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/contact_callbacks.dart';
import 'package:hello/classes/bodies/ball.dart';
import 'package:hello/classes/bodies/finish.dart';
import 'package:hello/classes/box2dworld.dart';

class BallFinishContactCallback extends ContactCallback<Ball, Finish> {
  @override
  void begin(Ball ball, Finish f, Contact contact) {
    ball.box2dWorld.curState = Box2dWorld.STATE_WON;
  }

  @override
  void end(Ball ball, Finish f, Contact contact) {}
}
