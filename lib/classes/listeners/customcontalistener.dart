import 'package:box2d_flame/box2d.dart';
import 'package:hello/classes/bodies/ball.dart';
import 'package:hello/classes/bodies/finish.dart';
import 'package:hello/classes/bodies/nocorrectionzone.dart';
import 'package:hello/classes/bodies/reversegravityzone.dart';
import 'package:hello/classes/bodies/wall.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/interfaces/killerbody.dart';
import 'package:hello/classes/interfaces/gravitiationaffected.dart';

class CustomContactListener implements ContactListener {
  @override
  void beginContact(Contact contact) {
    List listObjects = List()
      ..add(contact.fixtureA.getBody().userData)
      ..add(contact.fixtureB.getBody().userData);

    List<Type> listTypes = List()
      ..add(contact.fixtureA.getBody().userData.runtimeType)
      ..add(contact.fixtureB.getBody().userData.runtimeType);

    //ball interactions
    if (listTypes.contains(Ball)) {
      Ball b = getFromType(listObjects, Ball);
      dynamic otherBody = listObjects.firstWhere((element) => element != b);

      if (otherBody is KillerBody) {
        b.kill();
      } else if (otherBody.runtimeType == Finish) {
        b.box2dWorld.curState = Box2dWorld.STATE_WON;
      } else if (otherBody.runtimeType == Wall) {
        b.collide();
      } else if (otherBody.runtimeType == NoCorrectionZone) {
        b.isCorrectible = false;
      }
    }
    if (listTypes.contains(ReverseGravityZone)) {
      if (listObjects[0] is GravitationAffected ||
          listObjects[1] is GravitationAffected) {
        GravitationAffected ga =
            contact.fixtureA.getBody().userData is GravitationAffected
                ? contact.fixtureA.getBody().userData
                : contact.fixtureB.getBody().userData;
        ga.setGravityReversed(1);
      }
    }
  }

  @override
  void endContact(Contact contact) {
    List<dynamic> listObjects = List()
      ..add(contact.fixtureA.getBody().userData)
      ..add(contact.fixtureB.getBody().userData);

    List<Type> listTypes = List()
      ..add(contact.fixtureA.getBody().userData.runtimeType)
      ..add(contact.fixtureB.getBody().userData.runtimeType);

    if (listTypes.contains(Ball)) {
      Ball b = getFromType(listObjects, Ball);
      if (listTypes.contains(NoCorrectionZone)) {
        b.isCorrectible = true;
      }
    }
    if (listTypes.contains(ReverseGravityZone)) {
      if (listObjects[0] is GravitationAffected ||
          listObjects[1] is GravitationAffected) {
        GravitationAffected ga =
            contact.fixtureA.getBody().userData is GravitationAffected
                ? contact.fixtureA.getBody().userData
                : contact.fixtureB.getBody().userData;
        ga.setGravityReversed(-1);
      }
    }
  }

  @override
  void postSolve(Contact contact, ContactImpulse impulse) {
    // TODO: implement postSolve
  }

  @override
  void preSolve(Contact contact, Manifold oldManifold) {}

  dynamic getFromType(List<dynamic> listType, Type search) =>
      listType.firstWhere((element) => element.runtimeType == search);
}
