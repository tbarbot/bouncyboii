import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';

class LaserRayCastCallback extends RayCastCallback {
  BodyComponent body;
  double fractionFound;
  double reportFixture(
      Fixture fixture, Vector2 point, Vector2 normal, double fraction) {
    if (fixture.isSensor()) {
      return -1;
    }
    body = fixture.getBody().userData;
    fractionFound = fraction;
    return fraction;
  }
}
