import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/contact_callbacks.dart';
import 'package:hello/classes/bodies/reversegravityzone.dart';
import 'package:hello/classes/interfaces/gravitiationaffected.dart';

class BallreverseGravityContactCallback
    extends ContactCallback<GravitationAffected, ReverseGravityZone> {
  @override
  void begin(GravitationAffected ga, ReverseGravityZone rgz, Contact contact) {
    ga.setGravityReversed(1);
  }

  @override
  void end(GravitationAffected ga, ReverseGravityZone rgz, Contact contact) {
    ga.setGravityReversed(-1);
  }
}
