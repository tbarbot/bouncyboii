import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/contact_callbacks.dart';
import 'package:hello/classes/bodies/repulser.dart';
import 'package:hello/classes/interfaces/gravitiationaffected.dart';

class BallRepulserContactCallback
    extends ContactCallback<GravitationAffected, Repulser> {
  @override
  void begin(GravitationAffected ga, Repulser r, Contact contact) {
    ga.setRepulsion(r.body.position);
  }

  @override
  void end(GravitationAffected ga, Repulser r, Contact contact) {
    ga.removeRepulsion(r.body.position);
  }
}
