import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/contact_callbacks.dart';
import 'package:hello/classes/bodies/ball.dart';
import 'package:hello/classes/bodies/nocorrectionzone.dart';

class BallNoCorrectionZoneContactCallback
    extends ContactCallback<Ball, NoCorrectionZone> {
  @override
  void begin(Ball b, NoCorrectionZone rgz, Contact contact) {
    b.isCorrectible = false;
  }

  @override
  void end(Ball b, NoCorrectionZone rgz, Contact contact) {
    b.isCorrectible = true;
  }
}
