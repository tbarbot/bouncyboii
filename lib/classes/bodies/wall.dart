import 'dart:math';
import 'dart:ui' as ui;
import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/utils/palette.dart';
import 'package:yaml/yaml.dart';

class Wall extends BodyComponent {
  double x;
  double y;
  double w;
  double h;
  double angle = 0;
  double rotate = 0;
  int id;
  Paint wallPaint = Paint()..color = Palette().curentPalette.tertiary;
  Paint wallShadowPaint = Paint()
    ..color = (Colors.black..withOpacity(.9))
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 2);
  bool isTouched = false;
  bool shouldDestroy = false;

  Wall(
      {@required box2d,
      @required this.id,
      @required this.x,
      @required this.y,
      @required this.w,
      @required this.h,
      this.angle,
      this.rotate})
      : super(box2d) {
    _createBody();
  }

  static Wall fromMap(Box2dWorld world, YamlMap map) {
    final angleYml = map.containsKey('angle') ? map['angle'] : 0.0;
    final rotateYml = map.containsKey('rotate') ? map['rotate'] : 0.0;

    return Wall(
        box2d: world,
        id: map['id'],
        x: map['x'].runtimeType == double ? map['x'] : map['x'].toDouble(),
        y: map['y'].runtimeType == double ? map['y'] : map['y'].toDouble(),
        w: map['w'].runtimeType == double ? map['w'] : map['w'].toDouble(),
        h: map['h'].runtimeType == double ? map['h'] : map['h'].toDouble(),
        rotate:
            rotateYml.runtimeType == double ? rotateYml : rotateYml.toDouble(),
        angle: angleYml.runtimeType == double ? angleYml : angleYml.toDouble());
  }

  @override
  void renderPolygon(ui.Canvas canvas, List<ui.Offset> points) {
    final path = Path()..addPolygon(points, true);
    //wallPaint..blendMode = BlendMode.srcOut;
    canvas.drawPath(path, wallPaint);
  }

  @override
  void update(double t) {
    super.update(t);
  }

  void _createBody() {
    final shape = PolygonShape();
    shape.setAsBoxXY(w, h);
    final fixtureDef = FixtureDef();
    fixtureDef.shape = shape;

    fixtureDef.restitution = 0;
    fixtureDef.friction = 20;
    fixtureDef.filter.groupIndex = 2;

    final bodyDef = BodyDef();
    bodyDef.setUserData(this);
    bodyDef.position = Vector2(x, y);
    bodyDef.angle = toRadiant(angle);

    bodyDef.gravityScale = 0;
    if (rotate != 0) {
      bodyDef.type = BodyType.KINEMATIC;
    }
    Body groundBody = world.createBody(bodyDef);
    if (rotate != 0) {
      groundBody.setActive(true);
      groundBody.angularVelocity = rotate;
    }
    groundBody.createFixtureFromFixtureDef(fixtureDef);
    this.body = groundBody;
  }

  double toRadiant(double deg) {
    return deg * pi / 180;
  }

  @override
  int priority() => 8;
}
