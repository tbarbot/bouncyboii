import 'dart:math';
import 'dart:ui' as ui;

import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';
import 'package:flame/flame.dart';
import 'package:flutter/material.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/utils/palette.dart';

class TouchLine extends BodyComponent {
  ui.Image image;
  Color paint = Palette().curentPalette.secondary;
  Color colorFilter = Palette().curentPalette.primary;
  bool isLoading = true;
  Vector2 ballPosition;
  Offset td = Offset(0, 0);
  Vector2 position = Vector2(0, 0);
  bool shouldDestroy = false;
  bool maxLengthDetected = false;
  Path p;
  double width;
  double angle;
  Box2dWorld box2dWorld;
  Arrow arrow;

  static const double MAX_LINE_SIZE = 1.5;

  TouchLine(Box2dWorld box2d, Vector2 _ballPosition, Offset _td)
      : super(box2d) {
    ballPosition = _ballPosition;
    box2dWorld = box2d;
    td = _td;
    _loadImage();
    _createBody();
    _createArrow();
  }

  Vector2 get worldTd => this.viewport.getScreenToWorld(Vector2(td.dx, td.dy));

  void _loadImage() {
    Flame.images.load('circ.png').then((value) {
      image = value;
      isLoading = false;
    });
  }

  @override
  void renderPolygon(Canvas canvas, List<Offset> points) {
    if (!isLoading) {
      Offset vpCenterOffset = Offset(
          (points[0].dx + points[2].dx) / 2, (points[0].dy + points[2].dy) / 2);
      canvas.save();
      canvas.translate(vpCenterOffset.dx, vpCenterOffset.dy);
      canvas.rotate(-body.getAngle());
      paintImage(
          canvas: canvas,
          image: image,
          colorFilter: ColorFilter.mode(
              colorFilter.withOpacity(width / MAX_LINE_SIZE),
              BlendMode.srcATop),
          repeat: ImageRepeat.repeat,
          rect: Rect.fromCenter(
              width: width * 2 * Box2dWorld.VIEWPORT_SCALE,
              height: 0.1 * Box2dWorld.VIEWPORT_SCALE,
              center: Offset(0, 0)),
          fit: BoxFit.scaleDown);
      canvas.translate(-vpCenterOffset.dx, -vpCenterOffset.dy);
      canvas.restore();
    }
  }

  void _createBody() {
    final shape = PolygonShape();
    angle = computeAngle();
    width = computeLength();
    position = computePosition();
    shape.setAsBoxXY(width, .03);
    final fixtureDef = FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.isSensor = true;
    fixtureDef.filter.categoryBits = 0x0002;
    fixtureDef.filter.maskBits = 0x0002;
    fixtureDef.restitution = 0;
    fixtureDef.friction = 20;
    fixtureDef.filter.groupIndex = -8;

    final bodyDef = BodyDef();
    bodyDef.position = position;
    bodyDef.angle = angle;
    bodyDef.gravityScale = 0;
    this.body = world.createBody(bodyDef)
      ..createFixtureFromFixtureDef(fixtureDef);
  }

  void _createArrow() {
    arrow = Arrow(box, this);
  }

  @override
  void update(double t) {
    world.destroyBody(this.body);
    _createBody();
    super.update(t);
  }

  double computeLength() {
    double dx = worldTd.x - ballPosition.x;
    double dy = worldTd.y - ballPosition.y;
    double predictedLength = sqrt(dx * dx + dy * dy);
    predictedLength = predictedLength / 2;
    return predictedLength >= MAX_LINE_SIZE ? MAX_LINE_SIZE : predictedLength;
  }

  double computeAngle() {
    return atan2(ballPosition.y - (worldTd.y), ballPosition.x - (worldTd.x));
  }

  Vector2 computePosition() {
    Vector2 computedPoint = Vector2(ballPosition.x - ((width * 2) * cos(angle)),
        ballPosition.y - ((width * 2) * sin(angle)));
    return Vector2((computedPoint.x + ballPosition.x) / 2,
        (computedPoint.y + ballPosition.y) / 2);
  }

  @override
  int priority() => 9;

  @override
  bool destroy() {
    return shouldDestroy;
  }

  bool get debugMode => false;
}

class Arrow extends BodyComponent {
  TouchLine tl;
  ui.Image image;
  bool isLoading = false;
  static const double RADIUS = .2;
  Arrow(Box2DComponent box, this.tl) : super(box) {
    _loadImage();
    _createBody();
  }

  void _loadImage() {
    Flame.images.load('arrow.png').then((value) {
      image = value;
      isLoading = false;
    });
  }

  void _createBody() {
    final shape = new CircleShape();
    shape.radius = RADIUS;
    final activeFixtureDef = FixtureDef();
    activeFixtureDef.restitution = 1;
    activeFixtureDef.density = 1;
    activeFixtureDef.friction = 0;
    activeFixtureDef.shape = shape;
    activeFixtureDef.friction = 0;
    activeFixtureDef.filter.groupIndex = -8;
    activeFixtureDef.filter.categoryBits = 0x0002;
    activeFixtureDef.filter.maskBits = 0x0002;
    activeFixtureDef.isSensor = true;
    FixtureDef fixtureDef = activeFixtureDef;
    final activeBodyDef = BodyDef();
    activeBodyDef.setUserData(this);
    activeBodyDef.position = tl.ballPosition;
    activeBodyDef.type = BodyType.DYNAMIC;

    BodyDef bodyDef = activeBodyDef;

    body = world.createBody(bodyDef)..createFixtureFromFixtureDef(fixtureDef);
  }

  @override
  void renderCircle(Canvas canvas, Offset center, double radius) {
    if (isLoading || image == null) {
      return;
    }
    canvas.save();
    canvas.translate(center.dx, center.dy);
    canvas.rotate(-body.getAngle());

    paintImage(
        canvas: canvas,
        image: image,
        rect: new Rect.fromCircle(center: Offset(0, 0), radius: radius),
        fit: BoxFit.contain,
        colorFilter: ColorFilter.mode(
            Palette().curentPalette.secondary.withOpacity(1), BlendMode.srcIn));
    canvas.restore();
  }

  @override
  void update(double t) {
    body.setTransform(tl.ballPosition, tl.angle);
    super.update(t);
  }

  @override
  int priority() => 9;

  @override
  bool destroy() => tl.shouldDestroy;
}
