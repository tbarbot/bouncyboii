import 'dart:math';
import 'dart:ui' as ui;
import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';
import 'package:flutter/cupertino.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/utils/palette.dart';
import 'package:yaml/yaml.dart';

class ReverseGravityZone extends BodyComponent {
  double x;
  double y;
  double w;
  double h;
  int id;
  double angle = 0;
  Color paint = Palette().curentPalette.secondary;
  bool shouldDestroy = false;

  ReverseGravityZone(
      {@required box2d,
      @required this.id,
      @required this.x,
      @required this.y,
      @required this.w,
      @required this.angle,
      @required this.h})
      : super(box2d) {
    _createBody();
  }

  static ReverseGravityZone fromMap(Box2dWorld world, YamlMap map) {
    return ReverseGravityZone(
        box2d: world,
        id: map['id'],
        x: map['x'].toDouble(),
        y: map['y'].toDouble(),
        w: map['w'].toDouble(),
        angle: map['angle'].toDouble(),
        h: map['h'].toDouble());
  }

  @override
  void renderPolygon(ui.Canvas canvas, List<ui.Offset> points) {
    final path = Path()..addPolygon(points, true);
    canvas.drawPath(path, Paint()..color = paint.withAlpha(150));
  }

  @override
  void update(double t) {
    super.update(t);
  }

  void _createBody() {
    final shape = PolygonShape();
    shape.setAsBoxXY(w, h);
    final fixtureDef = FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.isSensor = true;

    fixtureDef.restitution = 0;
    fixtureDef.friction = 20;
    fixtureDef.filter.groupIndex = 2;

    final bodyDef = BodyDef();
    bodyDef.setUserData(this);
    bodyDef.position = Vector2(x, y);
    bodyDef.angle = toRadiant(angle);

    Body groundBody = world.createBody(bodyDef);
    groundBody.createFixtureFromFixtureDef(fixtureDef);

    this.body = groundBody;
  }

  double toRadiant(double deg) {
    return deg * pi / 180;
  }

  @override
  int priority() => 6;
}
