import 'dart:math';
import 'dart:ui' as ui;

import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';
import 'package:flutter/material.dart';
import 'package:hello/classes/bodies/saw.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/utils/const.dart';
import 'package:hello/classes/utils/palette.dart';
import 'package:yaml/yaml.dart';
import 'package:flutter/cupertino.dart';

class SawThrower extends BodyComponent {
  Vector2 pa = Vector2(0, 0);
  Vector2 pb = Vector2(0, 0);
  double speed = 300;
  double x;
  double y;
  double w = 0.0;
  Box2dWorld _box2d;
  Color paint = Palette().curentPalette.quaternary;
  int recurrency = 120;
  double currentRecurrencyCount = 0.0;
  bool shouldDestroy = false;
  ThrowingSaw _sawWaiting;
  static double CROP = .2;

  double get angle => atan2(pa.y - pb.y, pa.x - pb.x);

  SawThrower(Box2dWorld _box2dvar, double _x, double _y, double _r, Vector2 _pa,
      Vector2 _pb, double _speed, int _recurrency)
      : super(_box2dvar) {
    _box2d = _box2dvar;
    x = _x.toDouble();
    y = _y.toDouble();
    pa = _pa;
    pb = _pb;
    w = _r;
    speed = _speed;
    recurrency = _recurrency;
    _createBody();
  }

  static SawThrower fromMap(Box2dWorld world, YamlMap map) {
    return SawThrower(
        world,
        map['x'].toDouble(),
        map['y'].toDouble(),
        map['r'].toDouble(),
        Vector2(map['pax'].toDouble(), map['pay'].toDouble()),
        Vector2(map['pbx'].toDouble(), map['pby'].toDouble()),
        map['speed'].toDouble(),
        map['recurrency']);
  }

  @override
  void renderPolygon(ui.Canvas canvas, List<ui.Offset> points) {
    final path = Path()..addPolygon(points, true);
    canvas.drawPath(path, Paint()..color = paint);
  }

  void _createBody() {
    final shape = PolygonShape();
    shape.setAsBoxXY((w / 2) - CROP, (w / 2) - CROP);
    final fixtureDef = FixtureDef();
    fixtureDef.shape = shape;

    fixtureDef.restitution = 0;
    fixtureDef.friction = 20;
    fixtureDef.filter.groupIndex = 2;

    final bodyDef = BodyDef();
    bodyDef.setUserData(this);
    bodyDef.position = Vector2(x, y);
    bodyDef.angle = angle;
    bodyDef.gravityScale = 0;
    Body groundBody = world.createBody(bodyDef);
    groundBody.createFixtureFromFixtureDef(fixtureDef);
    this.body = groundBody;
    createBlade();
  }

  void throwBlade() {
    _sawWaiting.setVelocity();
    createBlade();
  }

  void createBlade() {
    ThrowingSaw s = ThrowingSaw(_box2d, x, y, .2, pa, pb, speed);
    _box2d.componentsToAdd.add(s);
    _sawWaiting = s;
  }

  void setWaitingSawRadius() {
    double scaled = (w / 2) * (currentRecurrencyCount / recurrency);
    _sawWaiting.setWidth(scaled);
  }

  @override
  void update(double t) {
    if (_box2d.curState != Box2dWorld.STATE_PAUSE) {
      currentRecurrencyCount +=
          _box2d.isBulletTiming ? 1 * Box2dWorld.SLOWMO_STEPPER : 1;
    }

    if (currentRecurrencyCount >= recurrency) {
      currentRecurrencyCount = 0;
      throwBlade();
    }
    setWaitingSawRadius();
    super.update(t);
  }

  @override
  int priority() => 9;
}

class ThrowingSaw extends Saw {
  Vector2 pa = Vector2(0, 0);
  Vector2 pb = Vector2(0, 0);
  double speed = 300;
  bool shouldDestroy = false;
  Box2dWorld _box2d;
  bool isWaiting = true;

  ThrowingSaw(Box2dWorld box2d, double _x, double _y, double _r, Vector2 _pa,
      Vector2 _pb, double _speed)
      : super(box2d, _x, _y, _r) {
    pa = _pa;
    pb = _pb;
    speed = _speed;
    _box2d = box2d;
  }

  double get angle {
    return atan2(pa.y - pb.y, pa.x - pb.x);
  }

  String get orientation {
    if (angle > 2) {
      return Const.ORIENTATION_LEFTTORIGHT;
    } else if (angle > 0) {
      return Const.ORIENTATION_TOPTOBOTTOM;
    } else if (angle > -1) {
      return Const.ORIENTATION_RIGHTTOLEFT;
    } else {
      return Const.ORIENTATION_BOTTOMTOTOP;
    }
  }

  void setVelocity() {
    body.linearVelocity = Vector2(-(cos(angle) * speed), -(sin(angle) * speed));
  }

  void setWidth(double w) {
    if (w > Saw.SAW_INDULGENCY) {
      w = w - Saw.SAW_INDULGENCY;
    }

    if (body != null) {
      Fixture f = body.getFixtureList();
      body.destroyFixture(f);
    }
    final fixtureDef = FixtureDef();
    final shape = CircleShape();
    shape.radius = w;
    fixtureDef.shape = shape;
    fixtureDef.restitution = .2;
    fixtureDef.filter.groupIndex = 2;
    body.createFixtureFromFixtureDef(fixtureDef);
  }

  bool checkShouldDestroy() {
    bool ret;
    switch (orientation) {
      case Const.ORIENTATION_LEFTTORIGHT:
        ret = body.position.x > pb.x;
        break;
      case Const.ORIENTATION_RIGHTTOLEFT:
        ret = body.position.x < pb.x;
        break;
      case Const.ORIENTATION_TOPTOBOTTOM:
        ret = -body.position.y > -pb.y;
        break;
      case Const.ORIENTATION_BOTTOMTOTOP:
        ret = -body.position.y < -pb.y;
        break;
    }
    return ret;
  }

  @override
  void update(double t) {
    if (checkShouldDestroy()) {
      _box2d.componentsToRemove.add(this);
    }
    super.update(t);
  }

  @override
  int priority() => 8;

  @override
  bool destroy() => shouldDestroy;
}
