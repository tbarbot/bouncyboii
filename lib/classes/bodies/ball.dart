import 'dart:math';
import 'dart:ui' as ui;

import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';
import 'package:flame/flame.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:hello/classes/bodies/repulser.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/interfaces/gravitiationaffected.dart';
import 'package:hello/classes/utils/audiomanager.dart';
import 'package:hello/classes/utils/palette.dart';

class Ball extends BodyComponent implements GravitationAffected {
  static const double RADIUS = .4;
  static const int COOLDOWN_FRAMES = 20;
  int coolDownCounter = 0;
  ui.Image image;
  double y;
  double x;
  Box2dWorld box2dWorld;
  bool shouldDestroy = false;
  bool isCorrectible = true;
  Paint paint = Paint()..color = Palette().curentPalette.primary;
  Paint coolDownPaint = Paint()..color = Palette().curentPalette.tertiary;
  AudioManager audioManager = AudioManager();
  Paint shadowPaint = Paint()
    ..color = Colors.red.withOpacity(BALL_SHADOW_OPACITY)
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 5);
  List<Vector2> currentRepulsionSource = List<Vector2>();

  @override
  bool isGravityReversed = false;
  @override
  int gravitationCount = 0;

  static const double LAUNCH_MULTIPLIER = 11;
  static const double BALL_DENSITY = 100;
  static const double BALL_RESTITUTION = .6;
  static const double BALL_SHADOW_OPACITY = .9;
  static const double BALL_SHADOW_ADITIONAL_RADIUS = 2;
  static const double REPULSER_CONST = 100;

  Ball(Box2dWorld _box2d, double _x, double _y) : super(_box2d) {
    box2dWorld = _box2d;
    y = _y.toDouble();
    x = _x.toDouble();
    _createBody();
  }

  @override
  void renderCircle(Canvas canvas, Offset center, double radius) {
    if (box2dWorld.isTouchLineActive) {
      canvas.drawCircle(center, radius + 2, shadowPaint);
    }
    canvas.drawCircle(
        center, radius, coolDownCounter > 0 ? coolDownPaint : paint);
  }

  @override
  void update(double t) {
    if (coolDownCounter > 0) {
      coolDownCounter--;
    }
    reverseGravitation(t);
    checkRepulsion(t);
  }

  void collide() {
    final rng = Random();
    if (box2dWorld.curState == Box2dWorld.STATE_PLAY) {
      int stuff = rng.nextInt(2) + 1;
      double totalVelocity =
          body.linearVelocity[0].abs() + body.linearVelocity[1].abs();
      double volume = totalVelocity / 20 > 1.0 ? 1.0 : totalVelocity / 20;
      Flame.audio.play('bounce${stuff.toString()}.wav', volume: volume);
    }
  }

  void checkRepulsion(double t) {
    if (currentRepulsionSource.isNotEmpty) {
      currentRepulsionSource.forEach((crs) {
        double repulsionAngle =
            atan2(body.position.y - crs.y, body.position.x - crs.x);
        double dx = body.position.x - crs.x;
        double dy = body.position.y - crs.y;
        double predictedLength = sqrt(dx * dx + dy * dy);
        double repulsionFactor =
            Repulser.DEFAULT_RADIUS + (RADIUS * 2) - predictedLength;
        double ix = repulsionFactor * REPULSER_CONST * cos(repulsionAngle);
        double iy = repulsionFactor * REPULSER_CONST * sin(repulsionAngle);
        body.applyLinearImpulse(Vector2(ix, iy), body.position, false);
      });
    }
  }

  void setGravityReverser(int flag) {
    gravitationCount += flag;
    isGravityReversed = gravitationCount > 0;
  }

  void setRepulsion(Vector2 r) {
    currentRepulsionSource.add(r);
  }

  void removeRepulsion(Vector2 r) {
    currentRepulsionSource.remove(r);
  }

  void kill() {
    if (box2dWorld.curState == Box2dWorld.STATE_PLAY) {
      shouldDestroy = true;
      box2dWorld.curState = Box2dWorld.STATE_DEATH;
      Flame.audio.play('blade.wav');
    }
  }

  void _createBody() {
    final shape = new CircleShape();
    shape.radius = RADIUS;
    final activeFixtureDef = FixtureDef();
    activeFixtureDef.shape = shape;
    activeFixtureDef.restitution = BALL_RESTITUTION;
    activeFixtureDef.density = BALL_DENSITY;
    activeFixtureDef.friction = 0;
    activeFixtureDef.filter.groupIndex = 2;
    activeFixtureDef.filter.categoryBits = 0x0001;
    activeFixtureDef.filter.maskBits = 0x0001;
    FixtureDef fixtureDef = activeFixtureDef;
    final activeBodyDef = BodyDef();
    activeBodyDef.linearDamping = .1;
    activeBodyDef.setUserData(this);
    activeBodyDef.position = Vector2(x, y);
    activeBodyDef.bullet = true;
    activeBodyDef.type = BodyType.DYNAMIC;

    BodyDef bodyDef = activeBodyDef;

    body = world.createBody(bodyDef)..createFixtureFromFixtureDef(fixtureDef);
  }

  void setReleaseVelocity(double angle, double length) {
    body.linearVelocity = Vector2(length * cos(angle) * LAUNCH_MULTIPLIER,
        length * sin(angle) * LAUNCH_MULTIPLIER);
  }

  @override
  bool destroy() {
    return shouldDestroy;
  }

  @override
  int priority() => 9;

  @override
  void reverseGravitation(double t) {
    if (isGravityReversed) {
      body.applyLinearImpulse(
          Vector2(0.0, BALL_DENSITY * 12 * t), body.position, false);
    }
  }

  @override
  void setGravityReversed(int flag) {
    gravitationCount += flag;
    isGravityReversed = gravitationCount > 0;
  }
}

class DeathParticles extends BodyComponent implements GravitationAffected {
  static const PARTICLE_LIFESPAN = 100;
  static const PARTICLE_RESTITUTION = .2;
  static const PARTICLE_DENSITY = 100.0;
  int lifeSpan = PARTICLE_LIFESPAN;
  Color paint = Palette().curentPalette.primary;
  Box2dWorld box2dWorld;
  bool isGravityReversed = false;
  int gravitationCount = 0;
  double y;
  double x;
  double width;
  bool shouldDestroy = false;
  List<Vector2> currentRepulsionSource = List<Vector2>();
  DeathParticles(
      {@required this.box2dWorld, @required this.x, @required this.y})
      : super(box2dWorld) {
    _createBody();
  }
  @override
  void renderPolygon(ui.Canvas canvas, List<ui.Offset> points) {
    final path = Path()..addPolygon(points, true);
    canvas.drawPath(path, Paint()..color = paint);
  }

  void _createBody() {
    final rng = Random();
    final shape = new PolygonShape();
    width = ((rng.nextDouble()) + 0.05) * .1;
    shape.setAsBoxXY(width, width);

    final activeFixtureDef = FixtureDef();
    activeFixtureDef.shape = shape;
    activeFixtureDef.restitution = PARTICLE_RESTITUTION;
    activeFixtureDef.density = PARTICLE_DENSITY;
    activeFixtureDef.friction = 50;
    activeFixtureDef.filter.groupIndex = 2;
    activeFixtureDef.filter.categoryBits = 0x0001;
    activeFixtureDef.filter.maskBits = 0x0001;
    FixtureDef fixtureDef = activeFixtureDef;
    final activeBodyDef = BodyDef();
    activeBodyDef.setUserData(this);

    activeBodyDef.linearVelocity =
        Vector2((rng.nextDouble() - .5) * 40, (rng.nextDouble() - .5) * 40);
    activeBodyDef.position = Vector2(x, y);
    activeBodyDef.type = BodyType.DYNAMIC;
    activeBodyDef.bullet = true;
    BodyDef bodyDef = activeBodyDef;

    body = world.createBody(bodyDef)..createFixtureFromFixtureDef(fixtureDef);
  }

  @override
  void update(double t) {
    reverseGravitation(t);
    checkRepulsion(t);
  }

  void checkRepulsion(double t) {
    if (currentRepulsionSource.isNotEmpty) {
      currentRepulsionSource.forEach((crs) {
        double repulsionAngle =
            atan2(body.position.y - crs.y, body.position.x - crs.x);

        double ix = 5 * cos(repulsionAngle);
        double iy = 5 * sin(repulsionAngle);
        body.applyLinearImpulse(Vector2(ix, iy), body.position, false);
      });
    }
  }

  @override
  bool destroy() {
    return shouldDestroy;
  }

  @override
  int priority() => 5;

  @override
  void reverseGravitation(double t) {
    if (isGravityReversed) {
      body.applyLinearImpulse(
          Vector2(0.0, PARTICLE_DENSITY * t * width * 7), body.position, false);
    }
  }

  @override
  void setGravityReversed(int flag) {
    gravitationCount += flag;
    isGravityReversed = gravitationCount > 0;
  }

  void setRepulsion(Vector2 r) {
    currentRepulsionSource.add(r);
  }

  void removeRepulsion(Vector2 r) {
    currentRepulsionSource.remove(r);
  }
}
