import 'dart:math';
import 'dart:ui' as ui;

import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';
import 'package:flame/flame.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hello/classes/bodies/saw.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/interfaces/killerbody.dart';
import 'package:hello/classes/utils/palette.dart';
import 'package:yaml/yaml.dart';

class MovingSaw extends Saw implements KillerBody {
  Vector2 pa = Vector2(0, 0);
  Vector2 pb = Vector2(0, 0);
  double speed = 0.0;
  bool isReturning = false;
  bool isVertical = false;
  MovingSawLine msl;
  bool shouldDestroy = false;
  bool velocitySet = false;

  double get angle => atan2(pa.y - pb.y, pa.x - pb.x);

  MovingSaw(box2d, double _x, double _y, double _r, Vector2 _pa, Vector2 _pb,
      double _speed, bool reversed)
      : super(box2d, _x, _y, _r) {
    pa = _pa;
    pb = _pb;
    speed = _speed;
    isReturning = reversed;
    isVertical = (angle > 1.4 && angle < 1.8);
    msl = MovingSawLine(box2d, pa, pb, angle);
  }

  static MovingSaw fromMap(Box2dWorld world, YamlMap map) {
    return MovingSaw(
        world,
        map['x'].toDouble(),
        map['y'].toDouble(),
        map['r'].toDouble(),
        Vector2(map['pax'].toDouble(), map['pay'].toDouble()),
        Vector2(map['pbx'].toDouble(), map['pby'].toDouble()),
        map['speed'].toDouble(),
        map['reversed']);
  }

  void setVelocity() {
    if (isReturning) {
      body.linearVelocity = Vector2((cos(angle) * speed), sin(angle) * speed);
    } else {
      body.linearVelocity =
          Vector2(-(cos(angle) * speed), -(sin(angle) * speed));
    }
  }

  void checkShouldReturn() {
    double begintestValue = isVertical ? -pa.y : pa.x;
    double endTestValue = isVertical ? -pb.y : pb.x;
    double bodyValue = isVertical ? -body.position.y : body.position.x;

    if (!isReturning) {
      if (endTestValue < bodyValue) {
        isReturning = !isReturning;
        setVelocity();
      }
    } else {
      if (begintestValue > bodyValue) {
        isReturning = !isReturning;
        setVelocity();
      }
    }
  }

  @override
  void update(double t) {
    if (!velocitySet) {
      velocitySet = true;
      setVelocity();
    }
    checkShouldReturn();
    super.update(t);
  }

  @override
  bool destroy() => shouldDestroy;
}

class MovingSawLine extends BodyComponent {
  Vector2 pa = Vector2(0, 0);
  Vector2 pb = Vector2(0, 0);
  double angle;
  Box2dWorld box2d;
  Color paint = Palette().curentPalette.quaternary;
  ui.Image image;
  bool isLoading = true;
  bool shouldDestroy = false;

  double get length =>
      sqrt((pa.x - pb.x) * (pa.x - pb.x) + (pa.y - pb.y) * (pa.y - pb.y)) / 2;

  MovingSawLine(this.box2d, this.pa, this.pb, this.angle) : super(box2d) {
    _loadImage();
    _createBody();
  }

  void _loadImage() {
    Flame.images.load('dash.png').then((value) {
      image = value;
      isLoading = false;
    });
  }

  void _createBody() {
    final shape = PolygonShape();
    final position = computePosition();
    shape.setAsBoxXY(length, .01);
    final fixtureDef = FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.filter.categoryBits = 0x0002;
    fixtureDef.filter.maskBits = 0x0002;
    fixtureDef.restitution = 0;
    fixtureDef.friction = 20;
    fixtureDef.filter.groupIndex = -8;

    final bodyDef = BodyDef();
    bodyDef.position = position;
    bodyDef.angle = angle;
    bodyDef.gravityScale = 0;
    bodyDef.setUserData(this);
    Body groundBody = world.createBody(bodyDef);
    groundBody.createFixtureFromFixtureDef(fixtureDef);
    this.body = groundBody;
  }

  @override
  void renderPolygon(Canvas canvas, List<Offset> points) {
    if (!isLoading) {
      Offset vpCenterOffset = Offset(
          (points[0].dx + points[2].dx) / 2, (points[0].dy + points[2].dy) / 2);
      canvas.save();
      canvas.translate(vpCenterOffset.dx, vpCenterOffset.dy);
      canvas.rotate(-body.getAngle());
      paintImage(
          canvas: canvas,
          image: image,
          repeat: ImageRepeat.repeat,
          colorFilter:
              ColorFilter.mode(Colors.black.withOpacity(.5), BlendMode.dstIn),
          rect: Rect.fromCenter(
              width: length * 2 * Box2dWorld.VIEWPORT_SCALE,
              height: 0.05 * Box2dWorld.VIEWPORT_SCALE,
              center: Offset(0, 0)),
          fit: BoxFit.scaleDown);
      canvas.translate(-vpCenterOffset.dx, -vpCenterOffset.dy);
      canvas.restore();
    }
  }

  Vector2 computePosition() {
    return Vector2((pa.x + pb.x) / 2, (pa.y + pb.y) / 2);
  }

  int priority() => 3;
}
