import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/utils/palette.dart';
import 'package:yaml/yaml.dart';

class Repulser extends BodyComponent {
  double x;
  double y;
  double w;
  double h;
  double radius = 0;
  static const double DEFAULT_RADIUS = 1;
  Color paint = Palette().curentPalette.secondary;
  bool shouldDestroy = false;

  Repulser(box2d, double _x, double _y, double _r) : super(box2d) {
    x = _x.toDouble();
    y = _y.toDouble();
    radius = 1.5;
    _createBody();
  }

  static Repulser fromMap(Box2dWorld world, YamlMap map) {
    final radiusYml = map.containsKey('r') ? map['r'] : DEFAULT_RADIUS;
    return Repulser(
        world, map['x'].toDouble(), map['y'].toDouble(), radiusYml.toDouble());
  }

  @override
  void renderCircle(Canvas canvas, Offset center, double radiusBody) {
    canvas.drawCircle(
        center, radiusBody, Paint()..color = paint.withAlpha(100));
  }

  void _createBody() {
    final shape = CircleShape();
    shape.radius = radius;
    final fixtureDef = FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.isSensor = true;
    fixtureDef.restitution = .2;
    fixtureDef.filter.groupIndex = 2;

    final bodyDef = BodyDef();
    bodyDef.setUserData(this);
    bodyDef.position = Vector2(x, y);
    bodyDef.angularVelocity = 20;
    bodyDef.type = BodyType.KINEMATIC;
    this.body = world.createBody(bodyDef)
      ..createFixtureFromFixtureDef(fixtureDef);
  }

  @override
  int priority() => 7;

  //@override
  bool get debugMode => true;
}
