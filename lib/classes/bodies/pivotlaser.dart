import 'dart:math';
import 'dart:ui' as ui;

import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';
import 'package:flame/flame.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/interfaces/killerbody.dart';
import 'package:hello/classes/listeners/laserraycastcallback.dart';
import 'package:hello/classes/utils/palette.dart';
import 'package:yaml/yaml.dart';

class PivotLaser extends BodyComponent {
  ui.Image image;
  bool isLoading = true;
  double x;
  double y;
  double w;
  double h;
  double angle = 0;
  double radius = .5;
  Color paint = Palette().curentPalette.secondary;
  Laser laser;
  Box2dWorld box2dWorld;
  Vector2 ballPosition;
  double angularVelocity = 0;
  bool shouldDestroy = false;
  int positiverotations = 0;
  int negativeRotations = 0;
  int rotations = 0;
  double prevAngleToAchieve;

  Vector2 get initialEnd {
    return Vector2(
        body.position.x - (laser.initialWidth * 2) * -cos(body.getAngle()),
        body.position.y - (laser.initialWidth * 2) * -sin(body.getAngle()));
  }

  Vector2 get realEnd => laser != null
      ? Vector2(body.position.x - ((laser.w * 2) * -cos(body.getAngle())),
          body.position.y - ((laser.w * 2) * -sin(body.getAngle())))
      : Vector2.zero();

  LaserRayCastCallback rccbl = LaserRayCastCallback();

  PivotLaser(Box2dWorld box2d, double _x, double _y) : super(box2d) {
    x = _x;
    y = _y;
    box2dWorld = box2d;
    _createBody();
    _loadImages();
    _createLaser();
  }

  static PivotLaser fromMap(Box2dWorld world, YamlMap map) {
    return PivotLaser(world, map['x'].toDouble(), map['y'].toDouble());
  }

  @override
  void renderCircle(Canvas canvas, Offset center, double radius) {
    if (isLoading) {
      return;
    }
    canvas.save();
    canvas.translate(center.dx, center.dy);
    canvas.rotate(-body.getAngle());

    paintImage(
        canvas: canvas,
        image: image,
        rect: new Rect.fromCircle(center: Offset(0, 0), radius: radius),
        fit: BoxFit.contain);
    canvas.restore();
  }

  void checkRotation(angleToAchieve) {
    if (prevAngleToAchieve != null) {
      if (prevAngleToAchieve < -3 && angleToAchieve > 3) {
        rotations--;
      }
      if (prevAngleToAchieve > 3 && angleToAchieve < -3) {
        rotations++;
      }
    }
  }

  @override
  void update(double t) {
    ballPosition =
        box2dWorld.ball != null ? box2dWorld.ball.body.position : null;

    if (ballPosition != null) {
      double angleToAchieve = computeAngleWithBall();
      checkRotation(angleToAchieve);
      prevAngleToAchieve = angleToAchieve;
      double computedAngleToAchieve = (rotations * pi) * 2 + (angleToAchieve);

      double totalRotation;
      if (rotations < 1) {
        totalRotation = (computedAngleToAchieve - body.getAngle());
      } else {
        totalRotation = (computedAngleToAchieve - body.getAngle());
      }
      angularVelocity = totalRotation * 5;
    } else {
      angularVelocity = 0;
    }
    body.angularVelocity = angularVelocity;
    rccbl.fractionFound = null;
    box2dWorld.world.raycast(rccbl, body.position, initialEnd);
    laser.w = rccbl.fractionFound != null
        ? laser.initialWidth * rccbl.fractionFound
        : laser.initialWidth;
    super.update(t);
  }

  double computeAngleWithBall() => atan2(
      ballPosition.y - (body.position.y), ballPosition.x - (body.position.x));

  Vector2 computePosition() => Vector2(
      (realEnd.x + body.position.x) / 2, (realEnd.y + body.position.y) / 2);

  void _loadImages() {
    Flame.images.load("pivotlaser.png").then((img) {
      image = img;
      isLoading = false;
    });
  }

  void _createBody() {
    final shape = CircleShape();
    shape.radius = radius;
    final fixtureDef = FixtureDef();
    fixtureDef.shape = shape;

    fixtureDef.restitution = .2;
    fixtureDef.filter.groupIndex = 2;

    final bodyDef = BodyDef();
    bodyDef.setUserData(this);
    bodyDef.position = Vector2(x, y);
    bodyDef.angle = 0;
    bodyDef.type = BodyType.KINEMATIC;
    this.body = world.createBody(bodyDef)
      ..createFixtureFromFixtureDef(fixtureDef);
  }

  void _createLaser() {
    laser = Laser(box2dWorld, this);
  }

  @override
  int priority() => 8;
}

class Laser extends BodyComponent implements KillerBody {
  double initialWidth = 40;
  double w = 40;
  double h;
  double radius = .5;
  PivotLaser pl;
  Color colorPrimary = Palette().curentPalette.secondary;
  Color colorSecondary = Palette().curentPalette.tertiary;
  Paint shadowPaint = Paint()
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 4);
  Box2dWorld box2dWorld;
  int paintTimer = 0;
  bool shouldDestroy = false;
  bool switchSecondColor = false;

  Laser(this.box2dWorld, this.pl) : super(box2dWorld) {
    _createBody();
  }

  @override
  void renderPolygon(ui.Canvas canvas, List<ui.Offset> points) {
    final path = Path()..addPolygon(points, true);
    Color laserColor = switchSecondColor ? colorPrimary : colorSecondary;
    Color shadowColor = switchSecondColor ? colorSecondary : colorPrimary;

    canvas.drawPath(path, shadowPaint..color = shadowColor);
    canvas.drawPath(path, Paint()..color = laserColor);
  }

  void _createBody() {
    final shape = PolygonShape();
    shape.setAsBoxXY(w, .03);
    final fixtureDef = FixtureDef();
    fixtureDef.shape = shape;
    fixtureDef.isSensor = true;

    final bodyDef = BodyDef();
    bodyDef.setUserData(this);
    bodyDef.position = pl.computePosition();
    bodyDef.angle = pl.body.getAngle();

    bodyDef.type = BodyType.KINEMATIC;
    Body groundBody = world.createBody(bodyDef);
    groundBody.createFixtureFromFixtureDef(fixtureDef);
    this.body = groundBody;
  }

  @override
  void update(double t) {
    world.destroyBody(this.body);
    _createBody();
    if (paintTimer % 2 == 0 && t != 0) {
      switchSecondColor = !switchSecondColor;
    }
    paintTimer++;
    super.update(t);
  }

  @override
  int priority() => 7;

  //@override
  bool get debugMode => true;
}
