import 'dart:math';

import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/painting.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/utils/palette.dart';

class Finish extends BodyComponent {
  double x;
  double y;
  static const double FINISH_RADIUS = .6;
  Paint paint = Paint()..color = Palette().curentPalette.quaternary;
  Paint shadowPaint = Paint()
    ..color = (Palette().curentPalette.quaternary..withOpacity(1))
    ..maskFilter = MaskFilter.blur(BlurStyle.solid, 10);
  Box2dWorld box2dWorld;
  bool shouldDestroy = false;

  Finish(box2d, double _x, double _y) : super(box2d) {
    x = _x.toDouble();
    y = _y.toDouble();
    box2dWorld = box2d;

    _createBody();
  }

  @override
  void update(double t) {
    super.update(t);
  }

  @override
  void renderCircle(Canvas canvas, Offset center, double radius) {
    canvas.drawCircle(center, radius, shadowPaint);
    canvas.drawCircle(center, radius, paint);
  }

  void _createBody() {
    final shape = CircleShape();
    shape.radius = FINISH_RADIUS;
    final fixtureDef = FixtureDef();
    fixtureDef.isSensor = true;
    fixtureDef.shape = shape;

    fixtureDef.filter.groupIndex = 2;

    final bodyDef = BodyDef();
    bodyDef.setUserData(this);
    bodyDef.position = Vector2(x, y);
    bodyDef.type = BodyType.STATIC;
    this.body = world.createBody(bodyDef)
      ..createFixtureFromFixtureDef(fixtureDef);
  }

  @override
  bool destroy() => shouldDestroy;

  @override
  int priority() => 0;
}

class WinParticles extends BodyComponent {
  static const PARTICLE_LIFESPAN = 100;
  static const PARTICLE_RESTITUTION = .2;
  static const PARTICLE_DENSITY = 1.0;
  static const double PARTICLE_RADIUS = .06;
  int lifeSpan = PARTICLE_LIFESPAN;
  Paint paint = Paint()..color = Palette().curentPalette.quaternary;
  Box2dWorld box2dWorld;
  double y;
  double x;
  bool shouldDestroy = false;
  WinParticles({@required this.box2dWorld, @required this.x, @required this.y})
      : super(box2dWorld) {
    _createBody();
  }

  @override
  void renderCircle(Canvas canvas, Offset center, double radius) {
    canvas.drawCircle(center, radius, paint);
  }

  void _createBody() {
    final rng = Random();
    final shape = new CircleShape();
    shape.radius = PARTICLE_RADIUS;
    shape.p.x = 0.0;

    final activeFixtureDef = FixtureDef();
    activeFixtureDef.shape = shape;
    activeFixtureDef.restitution = PARTICLE_RESTITUTION;
    activeFixtureDef.density = PARTICLE_DENSITY;
    activeFixtureDef.filter.groupIndex = -8;
    activeFixtureDef.filter.categoryBits = 0x0002;
    activeFixtureDef.filter.maskBits = 0x0002;
    FixtureDef fixtureDef = activeFixtureDef;
    final activeBodyDef = BodyDef();
    activeBodyDef.setUserData(this);

    activeBodyDef.linearVelocity =
        Vector2((rng.nextDouble() - .5) * 40, (rng.nextDouble() - .5) * 40);
    activeBodyDef.position = Vector2(x, y);
    activeBodyDef.type = BodyType.DYNAMIC;
    activeBodyDef.bullet = true;
    BodyDef bodyDef = activeBodyDef;

    body = world.createBody(bodyDef)..createFixtureFromFixtureDef(fixtureDef);
  }

  @override
  bool destroy() {
    return shouldDestroy;
  }

  @override
  int priority() => 5;
}
