import 'dart:math';
import 'dart:ui' as ui;
import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hello/classes/bodies/saw.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/utils/palette.dart';
import 'package:yaml/yaml.dart';

class SawPath extends BodyComponent {
  double x;
  double y;
  double w;
  double h;
  double angle = 0;
  double rotate = 0;
  int id;
  int sawVelocity;
  Box2dWorld _box2d;

  Paint wallPaint = Paint()..color = Palette().curentPalette.tertiary;
  Paint wallShadowPaint = Paint()
    ..color = (Colors.black..withOpacity(.9))
    ..maskFilter = MaskFilter.blur(BlurStyle.normal, 2);
  bool isTouched = false;
  bool shouldDestroy = false;
  YamlList connectedSaws;

  SawPath(
      {@required box2d,
      @required this.id,
      @required this.x,
      @required this.y,
      @required this.w,
      @required this.h,
      @required this.sawVelocity,
      this.connectedSaws,
      this.angle})
      : super(box2d) {
    this._box2d = box2d;
    _createBody();
    _createConnectedSaws();
  }

  static SawPath fromMap(Box2dWorld world, YamlMap map) {
    final angleYml = map.containsKey('angle') ? map['angle'] : 0.0;

    return SawPath(
        box2d: world,
        id: map['id'],
        x: map['x'].runtimeType == double ? map['x'] : map['x'].toDouble(),
        y: map['y'].runtimeType == double ? map['y'] : map['y'].toDouble(),
        w: map['w'].runtimeType == double ? map['w'] : map['w'].toDouble(),
        h: map['h'].runtimeType == double ? map['h'] : map['h'].toDouble(),
        sawVelocity: map['speed'],
        angle: angleYml.runtimeType == double ? angleYml : angleYml.toDouble(),
        connectedSaws: map['connectedSaws']);
  }

  @override
  void renderPolygon(ui.Canvas canvas, List<ui.Offset> points) {
    Offset vpCenterOffset = Offset(
        (points[0].dx + points[2].dx) / 2, (points[0].dy + points[2].dy) / 2);
    canvas.save();
    canvas.translate(vpCenterOffset.dx, vpCenterOffset.dy);
    Rect rect = Rect.fromCenter(
        width: w * 2 * Box2dWorld.VIEWPORT_SCALE,
        height: h * 2 * Box2dWorld.VIEWPORT_SCALE,
        center: Offset(0, 0));
    paintBorder(canvas, rect,
        left: BorderSide(
          width: 2.0,
          color: Palette().curentPalette.tertiary,
        ),
        right: BorderSide(
            width: 2,
            color: Palette().curentPalette.tertiary,
            style: BorderStyle.solid),
        bottom: BorderSide(width: 2.0, color: Palette().curentPalette.tertiary),
        top: BorderSide(width: 2.0, color: Palette().curentPalette.tertiary));
    canvas.restore();
  }

  @override
  void update(double t) {
    super.update(t);
  }

  void _createBody() {
    final shape = PolygonShape();
    shape.setAsBoxXY(w, h);
    final fixtureDef = FixtureDef();
    fixtureDef.shape = shape;

    fixtureDef.restitution = 0;
    fixtureDef.friction = 20;
    fixtureDef.isSensor = true;
    fixtureDef.filter.groupIndex = 2;

    final bodyDef = BodyDef();
    bodyDef.setUserData(this);
    bodyDef.position = Vector2(x, y);
    bodyDef.angle = toRadiant(angle);

    bodyDef.gravityScale = 0;
    if (rotate != 0) {
      bodyDef.type = BodyType.KINEMATIC;
    }
    Body groundBody = world.createBody(bodyDef);
    if (rotate != 0) {
      groundBody.setActive(true);
      groundBody.angularVelocity = rotate;
    }
    groundBody.createFixtureFromFixtureDef(fixtureDef);
    this.body = groundBody;
  }

  _createConnectedSaws() {
    connectedSaws.forEach((cs) {
      PathingSaw ps = PathingSaw(_box2d, cs['x'].toDouble(), cs['y'].toDouble(),
          cs['w'].toDouble(), cs['connectedSegment'], this);
      _box2d.componentsToAdd.add(ps);
    });
  }

  double toRadiant(double deg) {
    return deg * pi / 180;
  }

  @override
  int priority() => 4;
}

class PathingSaw extends Saw {
  bool shouldDestroy = false;
  Box2dWorld _box2d;
  SawPath st;
  int connectedPath = 0;
  List<Vector2> angleVectors = List();

  PathingSaw(Box2dWorld box2d, double _x, double _y, double _r,
      int _connectedPath, SawPath _st)
      : super(box2d, _x, _y, _r) {
    st = _st;
    connectedPath = _connectedPath;
    _box2d = box2d;
    setVectors();
    setVelocity();
  }

  void setVelocity() {
    double angleWithNext = atan2(
        body.position.y - angleVectors[connectedPath].y,
        body.position.x - angleVectors[connectedPath].x);
    body.linearVelocity = Vector2(-(cos(angleWithNext) * st.sawVelocity),
        -(sin(angleWithNext) * st.sawVelocity));
  }

  void setVectors() {
    angleVectors.add(Vector2(st.x - st.w, st.y + st.h));
    angleVectors.add(Vector2(st.x + st.w, st.y + st.h));
    angleVectors.add(Vector2(st.x + st.w, st.y - st.h));
    angleVectors.add(Vector2(st.x - st.w, st.y - st.h));
  }

  void checkIfStillConnected() {
    switch (connectedPath) {
      case 0:
        connectedPath = body.position.y >= angleVectors[0].y
            ? connectedPath + 1
            : connectedPath;
        setVelocity();
        break;
      case 1:
        connectedPath = body.position.x >= angleVectors[1].x
            ? connectedPath + 1
            : connectedPath;
        setVelocity();
        break;
      case 2:
        connectedPath = body.position.y <= angleVectors[2].y
            ? connectedPath + 1
            : connectedPath;
        setVelocity();
        break;
      case 3:
        connectedPath =
            body.position.x <= angleVectors[3].x ? 0 : connectedPath;
        setVelocity();
        break;
      default:
    }
  }

  @override
  void update(double t) {
    checkIfStillConnected();
    super.update(t);
  }

  @override
  bool destroy() => shouldDestroy;
}
