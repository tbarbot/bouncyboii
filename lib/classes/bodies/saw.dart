import 'dart:ui' as ui;

import 'package:box2d_flame/box2d.dart';
import 'package:flame/box2d/box2d_component.dart';
import 'package:flame/flame.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/interfaces/killerbody.dart';
import 'package:hello/classes/utils/palette.dart';
import 'package:yaml/yaml.dart';

class Saw extends BodyComponent implements KillerBody {
  ui.Image image;
  bool isLoading = true;
  double x;
  double y;
  double w;
  double h;
  double angle = 0;
  double radius = 0;
  static const double DEFAULT_RADIUS = 1;
  static const double SAW_INDULGENCY = .1;
  Color paint = Palette().curentPalette.secondary;
  bool shouldDestroy = false;

  Saw(box2d, double _x, double _y, double _r) : super(box2d) {
    x = _x.toDouble();
    y = _y.toDouble();
    radius = _r != null ? _r : DEFAULT_RADIUS;
    _loadImages();
    _createBody();
  }

  static Saw fromMap(Box2dWorld world, YamlMap map) {
    final radiusYml = map.containsKey('r') ? map['r'] : DEFAULT_RADIUS;
    return Saw(
        world, map['x'].toDouble(), map['y'].toDouble(), radiusYml.toDouble());
  }

  void _loadImages() {
    Flame.images.load("saw.png").then((img) {
      image = img;
      isLoading = false;
    });
  }

  @override
  void renderCircle(Canvas canvas, Offset center, double radiusBody) {
    if (isLoading) {
      return;
    }
    canvas.save();
    canvas.translate(center.dx, center.dy);
    canvas.rotate(-body.getAngle());

    paintImage(
        canvas: canvas,
        image: image,
        rect: new Rect.fromCircle(
            center: Offset(0, 0), radius: radiusBody + SAW_INDULGENCY * 60),
        fit: BoxFit.contain,
        colorFilter: ColorFilter.mode(
            Palette().curentPalette.secondary.withOpacity(1), BlendMode.srcIn));
    canvas.restore();
  }

  void _createBody() {
    final shape = CircleShape();
    shape.radius = radius - SAW_INDULGENCY;
    final fixtureDef = FixtureDef();
    fixtureDef.shape = shape;

    fixtureDef.restitution = .2;
    fixtureDef.filter.groupIndex = 2;

    final bodyDef = BodyDef();
    bodyDef.setUserData(this);
    bodyDef.position = Vector2(x, y);
    bodyDef.angularVelocity = 20;
    bodyDef.type = BodyType.KINEMATIC;
    this.body = world.createBody(bodyDef)
      ..createFixtureFromFixtureDef(fixtureDef);
  }

  @override
  int priority() => 7;

  //@override
  bool get debugMode => true;
}
