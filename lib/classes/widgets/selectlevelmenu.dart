import 'package:flutter/material.dart';
import 'package:hello/classes/model/chapter.dart';
import 'package:hello/classes/utils/const.dart';
import 'package:hello/classes/utils/palette.dart';
import 'package:hello/classes/utils/storage.dart';

typedef void IntCallback(int level);

class SelectLevelMenu extends StatelessWidget {
  final IntCallback onSelectLevel;
  final VoidCallback onCancel;
  final Map levelMap;
  final Chapter chapter;
  Storage storage = Storage();
  List<dynamic> storageStatus;

  SelectLevelMenu(
      {@required this.onSelectLevel,
      @required this.levelMap,
      @required this.chapter,
      @required this.onCancel}) {
    storageStatus = storage.getChapter(chapter);
  }

  bool checkLevelUnLocked(int level) {
    bool unlocked = storageStatus.elementAt(level).entries.elementAt(0).value;
    return unlocked;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Palette().curentPalette.secondary,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Expanded(
            child: Container(
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  itemCount: levelMap?.length ?? 0,
                  itemBuilder: (context, pos) => GestureDetector(
                      onTap: () => checkLevelUnLocked(pos)
                          ? onSelectLevel(pos)
                          : () => {},
                      child: Container(
                          decoration: BoxDecoration(
                            color: Colors.white,
                            image: DecorationImage(
                              image: AssetImage('assets/images/levels/1-1.png'),
                              fit: BoxFit.cover,
                            ),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black26,
                                  spreadRadius: 0,
                                  blurRadius: 5)
                            ],
                          ),
                          margin: EdgeInsets.symmetric(
                              horizontal: 15, vertical: 10),
                          child: Column(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              Container(
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    boxShadow: [
                                      BoxShadow(
                                        color: Palette().curentPalette.primary,
                                        blurRadius: 5,
                                      )
                                    ]),
                                margin: EdgeInsets.all(10),
                                padding: EdgeInsets.only(
                                    left: 15, right: 15, top: 15, bottom: 5),
                                child: Column(
                                  children: <Widget>[
                                    Padding(
                                      padding:
                                          const EdgeInsets.only(bottom: 10),
                                      child: Row(
                                        children: <Widget>[
                                          Text(
                                            levelMap[pos]['name'],
                                            style: Const.menutitlestyle,
                                          )
                                        ],
                                      ),
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          'level par',
                                          style: Const.menutextstyle,
                                        ),
                                        Text(
                                          'score',
                                          style: Const.menutextstyle,
                                        )
                                      ],
                                    ),
                                    Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        Text(
                                          levelMap[pos]['par'].toString(),
                                          style: Const.menutextstyle,
                                        ),
                                        Text(
                                          '5',
                                          style: Const.menutextstyle,
                                        )
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          width: 300))),
            ),
          ),
          Container(
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.end,
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                FlatButton(
                  shape: CircleBorder(),
                  color: Colors.white,
                  onPressed: onCancel,
                  child: Icon(
                    Icons.arrow_back,
                    color: Palette().curentPalette.primary,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
