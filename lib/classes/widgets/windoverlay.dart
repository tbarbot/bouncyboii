import 'package:flutter/material.dart';
import 'package:hello/classes/model/level.dart';
import 'package:hello/classes/utils/const.dart';
import 'package:hello/classes/utils/palette.dart';

class WinOverlay extends StatelessWidget {
  final VoidCallback onRestart;
  final VoidCallback onNext;
  final Level level;
  final int touchLineCount;
  final bool isLastLevel;

  WinOverlay(
      {@required this.onRestart,
      @required this.level,
      @required this.isLastLevel,
      @required this.onNext,
      @required this.touchLineCount});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Container(
        color: Palette().curentPalette.secondary.withAlpha(150),
        child: Align(
          child: Column(
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(top: 20, bottom: 20),
                child: Column(
                  children: <Widget>[
                    Text(
                      'Level complete :',
                      style: Const.menutextstyle,
                    ),
                    Text(
                      level.name,
                      style: Const.menutitlestyle,
                    ),
                  ],
                ),
              ),
              Table(defaultColumnWidth: FlexColumnWidth(1.0), children: [
                TableRow(children: [
                  Text(
                    'Level par',
                    style: Const.menutextstyle,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    'Your score',
                    style: Const.menutextstyle,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    'Best',
                    style: Const.menutextstyle,
                    textAlign: TextAlign.center,
                  ),
                ]),
                TableRow(children: [
                  Text(
                    level.par.toString(),
                    style: Const.menutitlestyle,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    touchLineCount.toString(),
                    style: Const.menutitlestyle,
                    textAlign: TextAlign.center,
                  ),
                  Text(
                    'xxxx',
                    style: Const.menutitlestyle,
                    textAlign: TextAlign.center,
                  ),
                ])
              ]),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  FlatButton(
                    onPressed: onRestart,
                    child: Text(
                      'Retry',
                      style: Const.menutextstyle,
                    ),
                  ),
                  FlatButton(
                    onPressed: onNext,
                    child: Text('Next Level', style: Const.menutextstyle),
                  ),
                ],
              )
            ],
          ),
          alignment: Alignment.center,
        ),
      ),
    );
  }
}
