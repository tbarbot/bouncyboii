import 'package:flutter/material.dart';
import 'package:hello/classes/utils/const.dart';

class DebugFrame extends StatelessWidget {
  final double frames;

  DebugFrame({@required this.frames});

  @override
  Widget build(BuildContext context) {
    TextStyle style =
        frames > 55 ? Const.overlaystyle : Const.overlaystylewarning;
    return Container(
      child: Align(
        child: Container(
          padding: EdgeInsets.all(10.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Text(frames.toStringAsFixed(3), style: style)
              ]),
        ),
        alignment: Alignment.bottomRight,
      ),
    );
  }
}
