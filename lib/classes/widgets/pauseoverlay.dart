import 'package:flutter/material.dart';
import 'package:hello/classes/utils/const.dart';
import 'package:hello/classes/utils/palette.dart';

class PauseOverlay extends StatelessWidget {
  final VoidCallback onPause;
  final VoidCallback onRestart;
  final VoidCallback onChooseLevel;
  final VoidCallback onChooseChapter;

  PauseOverlay(
      {@required this.onPause,
      @required this.onRestart,
      @required this.onChooseChapter,
      @required this.onChooseLevel});
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: Container(
        color: Palette().curentPalette.secondary.withAlpha(200),
        child: Align(
          child: Column(
            children: <Widget>[
              FlatButton(
                onPressed: onPause,
                child: Text(
                  'Resume',
                  style: Const.menutextstyle,
                ),
              ),
              FlatButton(
                onPressed: onRestart,
                child: Text(
                  'Restart',
                  style: Const.menutextstyle,
                ),
              ),
              FlatButton(
                onPressed: onChooseLevel,
                child: Text(
                  'Select level',
                  style: Const.menutextstyle,
                ),
              ),
              FlatButton(
                onPressed: onChooseChapter,
                child: Text(
                  'Select chapter',
                  style: Const.menutextstyle,
                ),
              ),
            ],
          ),
          alignment: Alignment.center,
        ),
      ),
    );
  }
}
