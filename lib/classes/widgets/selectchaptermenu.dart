import 'package:flutter/material.dart';
import 'package:hello/classes/model/chapter.dart';
import 'package:hello/classes/utils/const.dart';
import 'package:hello/classes/utils/palette.dart';
import 'package:hello/classes/utils/storage.dart';

typedef void ChapterCallback(Chapter level);

class SelectChapterMenu extends StatelessWidget {
  final ChapterCallback onSelectChapter;
  final ChapterCallback onSelectStagedChapter;
  final List<Chapter> allChapters;
  Storage storage = Storage();
  List<MapEntry> storageState;

  final controller = PageController();

  SelectChapterMenu(
      {@required this.onSelectChapter,
      @required this.allChapters,
      @required this.onSelectStagedChapter}) {
    storageState = storage.getChapters();
  }
  @override
  Widget build(BuildContext context) {
    if (storageState != null) {
      return Stack(
        children: <Widget>[
          PageView(
            children: buildViews(),
            controller: controller,
            onPageChanged: (index) => onSelectStagedChapter(allChapters[index]),
          ),
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Container(
                  child: TextButton(
                    onPressed: () => prevPage(),
                    child: Icon(
                      Icons.arrow_back,
                      color: Palette().curentPalette.primary,
                      size: 30.0,
                    ),
                  ),
                ),
                TextButton(
                  onPressed: () => nextPage(),
                  child: Icon(
                    Icons.arrow_forward,
                    color: Palette().curentPalette.primary,
                    size: 30.0,
                  ),
                ),
              ],
            ),
          )
        ],
      );
    }
  }

  void nextPage() async {
    await controller.nextPage(
        duration: Duration(milliseconds: 300), curve: Curves.ease);
  }

  void prevPage() async {
    await controller.previousPage(
        duration: Duration(milliseconds: 300), curve: Curves.ease);
  }

  List<Widget> buildViews() {
    List<Widget> views = [];
    allChapters.asMap().forEach((
      int index,
      Chapter f,
    ) {
      //omg this sucks ass
      bool isLocked = !storageState[index].value.entries.toList()[0].value;
      views.add(Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.fill,
              image: Image.asset(
                'assets/images/levels/1-1.png',
                fit: BoxFit.fill,
              ).image,
            ),
          ),
          child: Stack(
            children: [
              Center(
                child: GestureDetector(
                  onTap: !isLocked ? () => onSelectChapter(f) : () => {},
                  child: Container(
                    padding: EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                    color: Colors.white,
                    child: isLocked
                        ? Icon(
                            Icons.lock,
                            color: Palette().curentPalette.primary,
                            size: 30.0,
                          )
                        : Text(
                            '${f.name}',
                            style: Const.menutitlestyle,
                          ),
                  ),
                ),
              ),
            ],
          )));
    });
    return views;
  }
}
