import 'package:flutter/material.dart';
import 'package:hello/classes/utils/palette.dart';

class InGameLayout extends StatelessWidget {
  final VoidCallback onRestart;
  final VoidCallback onPause;
  final SingleChapterPalette palette;

  InGameLayout(
      {@required this.onPause,
      @required this.onRestart,
      @required this.palette});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Align(
        child: Container(
          padding: EdgeInsets.all(10.0),
          child:
              Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
            TextButton(
              onPressed: onPause,
              child: Icon(
                Icons.pause,
                color: palette.primary,
                size: 30.0,
              ),
            ),
            TextButton(
              onPressed: onRestart,
              child: Icon(
                Icons.replay,
                color: palette.primary,
                size: 30.0,
              ),
            ),
          ]),
        ),
        alignment: Alignment.bottomRight,
      ),
    );
  }
}
