import 'package:flutter/material.dart';
import 'package:hello/classes/utils/const.dart';

class DeathOverlay extends StatelessWidget {
  final VoidCallback onRestart;

  DeathOverlay({@required this.onRestart});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Align(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          padding: EdgeInsets.all(10.0),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                    onPressed: onRestart,
                    child: Text(
                      'tap to retry',
                      style: Const.overlaystyle,
                    )),
              ]),
        ),
        alignment: Alignment.bottomRight,
      ),
    );
  }
}
