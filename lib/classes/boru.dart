import 'package:flame/box2d/box2d_game.dart';
import 'package:flame/components/parallax_component.dart';
import 'package:flame/flame.dart';
import 'package:flame/game.dart';
import 'package:flame/gestures.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hello/classes/components/background.dart';
import 'package:hello/classes/listeners/ballfinishcontactcallback.dart';
import 'package:hello/classes/listeners/ballkillercontactcallback.dart';
import 'package:hello/classes/listeners/ballkwallcontactcallback.dart';
import 'package:hello/classes/listeners/ballnocorrectioncontactcallbak.dart';
import 'package:hello/classes/listeners/ballrepulsercontactcallback.dart';
import 'package:hello/classes/listeners/ballreversegravitycontactcallback.dart';
import 'package:hello/classes/model/chapter.dart';
import 'package:hello/classes/components/parallaxbg.dart';
import 'package:hello/classes/model/level.dart';
import 'package:hello/classes/box2dworld.dart';
import 'package:hello/classes/utils/audiomanager.dart';
import 'package:hello/classes/utils/const.dart';
import 'package:hello/classes/utils/palette.dart';
import 'package:hello/classes/utils/storage.dart';
import 'package:hello/classes/widgets/deathoverlay.dart';
import 'package:hello/classes/widgets/debugframe.dart';
import 'package:hello/classes/widgets/ingamelayout.dart';
import 'package:hello/classes/widgets/pauseoverlay.dart';
import 'package:hello/classes/widgets/selectchaptermenu.dart';
import 'package:hello/classes/widgets/selectlevelmenu.dart';
import 'package:hello/classes/widgets/windoverlay.dart';

class Boru extends Box2DGame
    with TapDetector, PanDetector, LongPressDetector, HasWidgetsOverlay {
  static const int DEATH_FRAMES_BEFORE_WIDGET = 60;
  static const bool DEBUG_MODE = true;

  Map levelMap;
  int curLevel;
  Chapter curChapter;
  int stagedChapter;
  Size screenSize = Size(0, 0);
  String stateBeforePause = Box2dWorld.STATE_PLAY;
  double frames = 0;
  ParallaxBg parallaxBg;
  AudioManager audioManager = AudioManager();
  Storage storage = Storage();
  SingleChapterPalette palette;
  Box2dWorld b2d;
  Boru(Box2dWorld box) : super(box) {
    this.b2d = box;
    addContactCallback(BallKillerContactCallback());
    addContactCallback(BallWallContactCallback());
    addContactCallback(BallFinishContactCallback());
    addContactCallback(BallreverseGravityContactCallback());
    addContactCallback(BallNoCorrectionZoneContactCallback());
    addContactCallback(BallRepulserContactCallback());
    Palette().curentChapter = 0;

    audioManager.loadAll(Const.audioFiles).then((a) {
      //audioManager.play('ATMOS.wav', .05);
    });
    Flame.audio.loadAll([
      'blade.wav',
      'whipe.wav',
      'bounce1.wav',
      'bounce2.wav',
    ]);
    // to test for now
    storage.deleteAll().then((value) =>
        storage.checkChaptersCreated().then((value) => onChooseChapter()));
  }

  @override
  void render(Canvas canvas) {
    super.render(canvas);
  }

  @override
  void update(double t) {
    if (b2d != null) {
      switch (b2d.curState) {
        case Box2dWorld.STATE_DEATH:
          if (b2d.deathFrames == DEATH_FRAMES_BEFORE_WIDGET) {
            addWidgetOverlay(
                'DeathOverlay',
                DeathOverlay(
                  onRestart: this.onRestart,
                ));
          }
          break;
        case Box2dWorld.STATE_WON:
          if (parallaxBg != null) {
            parallaxBg.shouldUpdate = false;
          }
          if (b2d.winFrames == 0) {
            storage
                .unlockNextLevel(curChapter.id, curLevel)
                .then((value) => null);
            storage
                .updateScore(curChapter.id, curLevel, b2d.touchLineCount)
                .then((value) => null);
          }
          if (b2d.winFrames == Box2dWorld.WIN_FRAMES) {
            b2d.curState = Box2dWorld.STATE_PAUSE;
            addWidgetOverlay(
                'WinOverlay',
                WinOverlay(
                  onRestart: this.onRestart,
                  onNext: this.onNextLevel,
                  level: b2d.level,
                  isLastLevel: false,
                  touchLineCount: b2d.touchLineCount,
                ));
          }
          break;
      }
    }

    if (DEBUG_MODE) {
      addWidgetOverlay('DebugFrame', DebugFrame(frames: super.fps()));
    }
    super.update(t);
  }

  void onRestart() {
    if (b2d.isPaused) {
      onPause();
    }
    removeWidgetOverlay('WinOverlay');
    loadLevel(curLevel);
  }

  ParallaxComponent generateParallax() {
    return parallaxBg = ParallaxBg(Const.images, box.viewport,
        layerDelta: Offset.zero, baseSpeed: Offset.zero);
  }

  void onNextLevel() {
    curLevel++;
    removeWidgetOverlay('WinOverlay');
    if (curChapter.levelMap.containsKey(curLevel)) {
      loadLevel(curLevel);
    } else {
      onChooseChapter();
    }
  }

  void onPause() {
    if (b2d.isPaused) {
      b2d.curState = stateBeforePause;
      removeWidgetOverlay('PauseOverlay');
    } else {
      stateBeforePause = b2d.curState;
      b2d.curState = Box2dWorld.STATE_PAUSE;
      addWidgetOverlay(
          'PauseOverlay',
          PauseOverlay(
              onPause: this.onPause,
              onRestart: this.onRestart,
              onChooseChapter: this.onChooseChapter,
              onChooseLevel: this.onChooseLevel));
    }
    b2d.isPaused = !b2d.isPaused;
  }

  void onChooseChapter() {
    Palette().curentChapter = 0;
    addWidgetOverlay(
        'SelectChapterMenu',
        SelectChapterMenu(
          onSelectChapter: this.onSelectChapter,
          onSelectStagedChapter: this.selectStagedChapter,
          allChapters: Const.chapters,
        ));
  }

  void onChooseLevel() {
    addWidgetOverlay(
        'SelectLevelMenu',
        SelectLevelMenu(
          onSelectLevel: this.onSelectLevel,
          levelMap: curChapter.levelMap,
          onCancel: this.onChooseChapter,
          chapter: curChapter,
        ));
  }

  void onSelectLevel(int level) {
    curLevel = level;
    loadLevel(curLevel);
    removeWidgetOverlay('SelectLevelMenu');
    removeWidgetOverlay('PauseOverlay');
  }

  void onSelectChapter(Chapter chapter) {
    if (chapter.levelResolved) {
      curChapter = chapter;
      Palette().curentChapter = chapter.id - 1;
      palette = Palette().curentPalette;
      removeWidgetOverlay('SelectChapterMenu');
      onChooseLevel();
    }
  }

  void selectStagedChapter(Chapter chapter) {
    curChapter = chapter;
    Palette().curentChapter = chapter.id - 1;
  }

  @override
  void resize(Size size) {
    box.resize(size);
    super.resize(size);
  }

  void loadLevel(int level) {
    removeWidgetOverlay('DeathOverlay');
    removeWidgetOverlay('InGameLayout');
    addWidgetOverlay(
        'InGameLayout',
        InGameLayout(
            onPause: this.onPause,
            onRestart: this.onRestart,
            palette: palette));
    destroyLevel();
    Level l = Level(level, curChapter.levelMap[level], box);
    b2d.level = l;
    b2d.initializeWorld();
    //generateParallax();
    add(BackGroundComponent());
    //add(WallTextureComponent());
  }

  void destroyLevel() {
    if (parallaxBg != null) {
      parallaxBg.shouldDestroy = true;
    }
    box.components.forEach((c) {
      box.world.destroyBody(c.body);
    });
    box.components.clear();
  }

  // gesture stuff
  void onTapDown(TapDownDetails details) {
    b2d.touchOffset = details.globalPosition;
    if (b2d.verifyTouchOffset(details.globalPosition)) {
      b2d.createTouchLine(details);
    }
  }

  void onPanDown(DragDownDetails details) {
    b2d.touchOffset = details.globalPosition;
    if (b2d.isTouchLineActive ||
        b2d.verifyTouchOffset(details.globalPosition)) {
      b2d.createTouchLine(details);
    }
  }

  void onTapUp(TapUpDetails details) {
    b2d.setBallVelocity();
    b2d.removeTouchLine();
  }

  void onPanStart(DragStartDetails details) {
    b2d.touchOffset = details.globalPosition;
    if (b2d.isTouchLineActive ||
        b2d.verifyTouchOffset(details.globalPosition)) {
      b2d.createTouchLine(details);
    }
  }

  void onPanUpdate(DragUpdateDetails details) {
    b2d.touchOffset = details.globalPosition;
    if (b2d.isTouchLineActive) {
      b2d.createTouchLine(details);
    }
  }

  void onPanEnd(DragEndDetails details) {
    b2d.setBallVelocity();
    b2d.removeTouchLine();
  }

  void onLongPressStart(LongPressStartDetails details) {
    b2d.touchOffset = details.globalPosition;
    b2d.createTouchLine(details);
  }

  void onLongPressMoveUpdate(LongPressMoveUpdateDetails details) {
    b2d.touchOffset = details.globalPosition;
    b2d.createTouchLine(details);
  }

  void onLongPressEnd(LongPressEndDetails details) {
    b2d.setBallVelocity();
    b2d.removeTouchLine();
  }

  @override
  bool recordFps() {
    return false;
  }
}
