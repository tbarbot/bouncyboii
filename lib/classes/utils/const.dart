import 'package:flame/components/parallax_component.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hello/classes/model/audio.dart';
import 'package:hello/classes/model/chapter.dart';
import 'package:hello/classes/utils/palette.dart';

class Const {
  static TextStyle menutextstyle = TextStyle(
      fontFamily: 'Gasalt',
      color: Palette().curentPalette.primary,
      fontSize: 25);

  static TextStyle menutitlestyle = TextStyle(
      fontFamily: 'Gasalt',
      color: Palette().curentPalette.primary,
      fontSize: 30);

  static TextStyle overlaystyle = TextStyle(
      fontFamily: 'Gasalt',
      color: Palette().curentPalette.primary,
      fontSize: 50);

  static TextStyle overlaystylewarning =
      TextStyle(fontFamily: 'Gasalt', color: Colors.redAccent, fontSize: 50);

  static final List<Chapter> chapters = <Chapter>[
    Chapter(
      id: 1,
      name: 'Newbies playground',
    ),
    Chapter(id: 2, name: 'Cutting Workshop'),
    Chapter(id: 3, name: 'Aztec temple'),
    Chapter(id: 4, name: 'The lab'),
    Chapter(id: 5, name: 'Space'),
  ];

  static final List<Audio> audioFiles = <Audio>[
    Audio('ATMOS.wav', true, 1.0),
  ];

  static const String ORIENTATION_LEFTTORIGHT = 'lefttoright';
  static const String ORIENTATION_RIGHTTOLEFT = 'righttoleft';
  static const String ORIENTATION_TOPTOBOTTOM = 'toptobottom';
  static const String ORIENTATION_BOTTOMTOTOP = 'bottomtotop';

  static final List<ParallaxImage> images = [
    ParallaxImage("parallax/bg.png",
        fill: LayerFill.height,
        alignment: Alignment.bottomCenter,
        repeat: ImageRepeat.repeatX),
    ParallaxImage("parallax/bg1.png",
        fill: LayerFill.height,
        alignment: Alignment.bottomCenter,
        repeat: ImageRepeat.repeatX),
    ParallaxImage("parallax/bg2.png",
        fill: LayerFill.height,
        alignment: Alignment.bottomCenter,
        repeat: ImageRepeat.repeatX),
    ParallaxImage("parallax/bg3.png",
        fill: LayerFill.height,
        alignment: Alignment.bottomCenter,
        repeat: ImageRepeat.repeatX),
    /*ParallaxImage("parallax/foreground-trees.png",
        fill: LayerFill.height,
        alignment: Alignment.bottomCenter,
        repeat: ImageRepeat.repeatX),*/
  ];
}
