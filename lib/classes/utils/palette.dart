import 'package:flutter/cupertino.dart';

class Palette {
  static final Palette _singleton = Palette._internal();

  factory Palette() {
    return _singleton;
  }

  Palette._internal();
  List<SingleChapterPalette> chapters = [
    SingleChapterPalette(
        primary: Color.fromARGB(255, 35, 25, 66),
        secondary: Color.fromARGB(255, 94, 84, 142),
        tertiary: Color.fromARGB(255, 159, 134, 192),
        quaternary: Color.fromARGB(255, 190, 149, 196),
        quinary: Color.fromARGB(255, 224, 177, 203)),
    SingleChapterPalette(
        primary: Color.fromARGB(255, 35, 25, 66),
        secondary: Color.fromARGB(255, 94, 84, 142),
        tertiary: Color.fromARGB(255, 159, 134, 192),
        quaternary: Color.fromARGB(255, 190, 149, 196),
        quinary: Color.fromARGB(255, 224, 177, 203)),
    SingleChapterPalette(
        primary: Color.fromARGB(255, 35, 25, 66),
        secondary: Color.fromARGB(255, 94, 84, 142),
        tertiary: Color.fromARGB(255, 159, 134, 192),
        quaternary: Color.fromARGB(255, 190, 149, 196),
        quinary: Color.fromARGB(255, 224, 177, 203)),
    SingleChapterPalette(
        primary: Color.fromARGB(255, 35, 25, 66),
        secondary: Color.fromARGB(255, 94, 84, 142),
        tertiary: Color.fromARGB(255, 159, 134, 192),
        quaternary: Color.fromARGB(255, 190, 149, 196),
        quinary: Color.fromARGB(255, 224, 177, 203)),
    SingleChapterPalette(
        primary: Color.fromARGB(255, 35, 25, 66),
        secondary: Color.fromARGB(255, 94, 84, 142),
        tertiary: Color.fromARGB(255, 159, 134, 192),
        quaternary: Color.fromARGB(255, 190, 149, 196),
        quinary: Color.fromARGB(255, 224, 177, 203)),
    SingleChapterPalette(
        primary: Color.fromARGB(255, 35, 25, 66),
        secondary: Color.fromARGB(255, 94, 84, 142),
        tertiary: Color.fromARGB(255, 159, 134, 192),
        quaternary: Color.fromARGB(255, 190, 149, 196),
        quinary: Color.fromARGB(255, 224, 177, 203)),
  ];
  int currentChapter;
  SingleChapterPalette get curentPalette => chapters[currentChapter];

  set curentChapter(int index) {
    currentChapter = index;
  }
}

class SingleChapterPalette {
  Color primary;
  Color secondary;
  Color tertiary;
  Color quaternary;
  Color quinary;

  SingleChapterPalette(
      {this.primary,
      this.secondary,
      this.tertiary,
      this.quaternary,
      this.quinary});
}
