import 'package:hello/classes/model/audio.dart';
import 'package:ocarina/ocarina.dart';

class AudioManager {
  static const DEFAULT_DIRECTORY = 'assets/audio/';
  List<OcarinaPlayer> audiosLoaded = List();
  bool loaded = false;
  static final AudioManager _singleton = AudioManager._internal();

  factory AudioManager() {
    return _singleton;
  }

  AudioManager._internal();

  Future<void> loadAll(List<Audio> audios) async {
    for (Audio audio in audios) {
      await load(audio);
    }
    loaded = audiosLoaded.length > 0;
  }

  Future<OcarinaPlayer> load(Audio audio) async {
    OcarinaPlayer op = OcarinaPlayer(
        asset: DEFAULT_DIRECTORY + audio.file,
        loop: audio.loop,
        volume: audio.volume);
    await op.load();
    op.updateVolume(audio.volume);
    audiosLoaded.add(op);
    return op;
  }

  void clearAll(List<String> audios) {
    audiosLoaded = List();
    loaded = false;
  }

  void play(String filename, [double volume = 1, bool loop = false]) {
    if (loaded) {
      OcarinaPlayer op = audiosLoaded
          .singleWhere((op) => op.asset == DEFAULT_DIRECTORY + filename);
      op.updateVolume(volume);
      op.play();
    }
  }

  void pause(String filename) {
    if (loaded) {
      OcarinaPlayer op = audiosLoaded
          .singleWhere((op) => op.asset == DEFAULT_DIRECTORY + filename);
      op.pause();
    }
  }
}
