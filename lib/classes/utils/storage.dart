import 'dart:async';
import 'dart:convert';

import 'package:hello/classes/model/chapter.dart';
import 'package:hello/classes/model/storage/storagelevel.dart';
import 'package:localstore/localstore.dart';

class Storage {
  static final Storage _singleton = Storage._internal();
  static final String TBL_NAME = 'chapters';
  static final List<int> CHAPTERS = [0, 1, 2, 3, 4];
  static final List<int> LEVELS = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

  final db = Localstore.instance;

  factory Storage() {
    return _singleton;
  }

  Storage._internal();

  Map<String, dynamic> _state;
  bool lodaded = false;

  Future<void> checkChaptersCreated() async {
    return db.collection(TBL_NAME).doc('1').get().then((value) async {
      if (value == null) {
        await createChapters();
        await _setChapters();
      } else {
        await _setChapters();
        lodaded = true;
      }
    });
  }

  Future<void> createChapters() {
    List<StorageLevel> levels = [];
    LEVELS.forEach((level) {
      levels.add(StorageLevel(false, null));
    });
    CHAPTERS.forEach((element) async {
      await db.collection(TBL_NAME).doc(element.toString()).set({
        'unlocked': element == 0 ? true : false,
        'levels': jsonEncode(levels)
      });
      await unlockFirstLevel();
    });
    lodaded = true;
    return null;
  }

  Future<void> unlockFirstLevel() async {
    return db.collection(TBL_NAME).doc('0').get().then((value) async {
      List<dynamic> levels = jsonDecode(value['levels']);
      levels[0]['unlocked'] = true;
      await db
          .collection(TBL_NAME)
          .doc('0')
          .set({'unlocked': true, 'levels': jsonEncode(levels)});
    });
  }

  Future<Map<String, dynamic>> getLevelsForChapter(int id) {
    return db.collection(TBL_NAME).doc(id.toString()).get();
  }

  Future<Map<String, dynamic>> _setChapters() async {
    return await db.collection(TBL_NAME).get().then((value) => _state = value);
  }

  /// just for test
  Future<void> deleteAll() {
    Completer completer = new Completer();
    CHAPTERS.forEach((int chap) async {
      await db.collection(TBL_NAME).doc(chap.toString()).delete();
    });
    completer.complete();
    return completer.future;
  }

  //spaghetti shit needs refacto asap
  Future unlockNextLevel(int chapter, int level) {
    Completer completer = new Completer();
    db.collection(TBL_NAME).doc((chapter - 1).toString()).get().then((value) {
      List<dynamic> levels = jsonDecode(value['levels']);
      if (levels.length - 1 > level) {
        //unlock next level
        levels[level + 1]['unlocked'] = true;
        db
            .collection(TBL_NAME)
            .doc((chapter - 1).toString())
            .set({'unlocked': true, 'levels': jsonEncode(levels)}).then(
                (value) async {
          completer.complete();
          await _setChapters();
        });
      } else {
        //unlock next chapter
        db.collection(TBL_NAME).doc((chapter).toString()).get().then((chap) {
          if (chap != null) {
            List<dynamic> levels = jsonDecode(value['levels']);
            levels[0]['unlocked'] = true;
            db
                .collection(TBL_NAME)
                .doc((chapter).toString())
                .set({'unlocked': true, 'levels': jsonEncode(levels)}).then(
                    (value) async {
              completer.complete();
              await _setChapters();
            });
          } else {
            // end of the game reached here
          }
        });
      }
    });
    return completer.future;
  }

  Future updateScore(int chapter, int level, int score) {
    Completer completer = new Completer();
    db.collection(TBL_NAME).doc((chapter - 1).toString()).get().then((value) {
      List<dynamic> levels = jsonDecode(value['levels']);
      levels[level]['best'] =
          levels[level]['best'] > score ? score : levels[level]['best'];
      db.collection(TBL_NAME).doc((chapter - 1).toString()).set(
          {'unlocked': true, 'levels': jsonEncode(levels)}).then((value) async {
        completer.complete();
        await _setChapters();
      });
    });

    return completer.future;
  }

  Map<String, dynamic> getState() => _state;

  ///get all chapters
  List getChapters() {
    List<MapEntry> chapters = _state.entries.toList();
    chapters.sort((a, b) => int.parse(a.key.substring(a.key.length - 1)) >
            int.parse(b.key.substring(b.key.length - 1))
        ? 1
        : -1);
    return chapters;
  }

  ///get one chapter
  List<dynamic> getChapter(Chapter chapter) =>
      jsonDecode(_state['/chapters/' + (chapter.id - 1).toString()]
          .entries
          .elementAt(1)
          .value);
}
