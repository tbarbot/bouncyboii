import 'package:flame/box2d/box2d_component.dart';
import 'package:flame/flame.dart';
import 'package:box2d_flame/box2d.dart';
import 'package:flutter/cupertino.dart';
import 'package:hello/classes/bodies/finish.dart';
import 'package:hello/classes/bodies/touchline.dart';
import 'package:hello/classes/bodies/ball.dart';
import 'package:hello/classes/model/level.dart';

class Box2dWorld extends Box2DComponent {
  static const double VIEWPORT_SCALE = 60;
  static const double SLOWMO_STEPPER = .05;
  static const int RESPAWN_TIME = 1500;
  static const int WIN_FRAMES = 60;

  static const String STATE_DEATH = 'DEAD';
  static const String STATE_PAUSE = 'PAUSE';
  static const String STATE_PLAY = 'PLAY';
  static const String STATE_WON = 'WON';

  Box2dWorld() : super(scale: VIEWPORT_SCALE, gravity: -12.0);
  Ball ball;
  TouchLine tl;
  Size screenSize;
  bool isTouchLineActive = false;
  bool isWinning = false;
  bool isPaused = false;
  bool isBulletTiming = false;
  Offset touchOffset = Offset(0, 0);
  int touchLineCount = 0;
  int winFrames = 0;
  int deathFrames = 0;
  Level level;
  List<DeathParticles> deathParticles = List();
  List<WinParticles> winParticles = List();
  Finish finish;
  String curState = STATE_PAUSE;
  List<BodyComponent> componentsToAdd = [];
  List<BodyComponent> componentsToRemove = [];

  @override
  void initializeWorld() {
    isWinning = false;
    isBulletTiming = false;
    winFrames = 0;
    deathFrames = 0;
    touchLineCount = 0;
    isPaused = false;
    viewport.scale = VIEWPORT_SCALE;
    curState = STATE_PLAY;
    initializeBodies();
    add(ball = Ball(this, level.spawn.dx, level.spawn.dy));
    cameraFollow(ball, horizontal: 0, vertical: 0);
    velocityIterations = 3;
    positionIterations = 8;
  }

  void initializeBodies() {
    level.bodies.forEach((b) {
      if (b.runtimeType == Finish) {
        finish = b as Finish;
      }
      add(b);
    });
  }

  @override
  void resize(Size size) {
    screenSize = size;
    super.resize(size);
  }

  @override
  void update(t) {
    if (componentsToRemove.length > 0) {
      componentsToRemove.forEach((element) => remove(element));
    }
    componentsToRemove = [];
    if (componentsToAdd.length > 0) {
      componentsToAdd.forEach((element) => add(element));
    }
    componentsToAdd = [];
    switch (curState) {
      case STATE_PLAY:
        cameraFollow(ball, horizontal: 0.6, vertical: 0);
        if (isBulletTiming) {
          t = t * SLOWMO_STEPPER;
          refreshTouchLine();
        }
        break;

      case STATE_DEATH:
        isBulletTiming = false;
        if (deathFrames < 40) {
          isBulletTiming = true;
          t = t * SLOWMO_STEPPER;
        }
        deathFrames++;
        break;

      case STATE_WON:
        cameraFollow(ball, horizontal: 0.6, vertical: 0);
        if (winFrames == 0) {
          finishAnimation();
        }
        double finishValue = 2 + VIEWPORT_SCALE;
        double tAnimation = (winFrames / WIN_FRAMES);
        if (winFrames < WIN_FRAMES) {
          viewport.scale =
              -finishValue / 2 * ((tAnimation) * (tAnimation - 2)) +
                  VIEWPORT_SCALE;

          winFrames++;
          t = t * SLOWMO_STEPPER;
        } else {
          t = 0;
        }
        break;

      case STATE_PAUSE:
        t = 0;
        break;
    }

    if (ball != null && ball.shouldDestroy) {
      kill();
    }
    super.update(t);
  }

  void kill() {
    generateParticles();
    ball.shouldDestroy = true;
    ball = null;
    if (tl != null) {
      removeTouchLine();
    }
  }

  generateParticles() {
    for (int i = 0; i < 10; i++) {
      deathParticles.add(DeathParticles(
          box2dWorld: this, x: ball.body.position.x, y: ball.body.position.y));
    }

    deathParticles.forEach((dp) => add(dp));
    deathParticles = List();
  }

  void finishAnimation() {
    Flame.audio.play('whipe.wav', volume: .6);
    finish.shouldDestroy = true;

    for (int i = 0; i < 10; i++) {
      winParticles.add(WinParticles(
          box2dWorld: this,
          x: finish.body.position.x,
          y: finish.body.position.y));
    }
    winParticles.forEach((wp) => add(wp));
    winParticles = List();
  }

  bool verifyTouchOffset(Offset o) {
    if (ball != null) {
      Vector2 to = this.viewport.getScreenToWorld(Vector2(o.dx, o.dy));
      double distanceSquared =
          (to.x - ball.body.position.x) * (to.x - ball.body.position.x) +
              (to.y - ball.body.position.y) * (to.y - ball.body.position.y);

      return distanceSquared <= Ball.RADIUS * 10;
    }
    return false;
  }

  void createTouchLine(details) {
    if (ball.isCorrectible) {
      if (null == tl &&
          null != ball &&
          ball.coolDownCounter == 0 &&
          curState == STATE_PLAY) {
        isTouchLineActive = isBulletTiming = true;
        tl = TouchLine(this, ball.body.position, touchOffset);
        add(tl);
        add(tl.arrow);
      }
    }
  }

  void setBallVelocity() {
    if (ball != null && isTouchLineActive) {
      touchLineCount++;
      ball.setReleaseVelocity(tl.angle, tl.width);
      ball.coolDownCounter = Ball.COOLDOWN_FRAMES;
    }
  }

  void refreshTouchLine() {
    if (null != tl && null != ball) {
      tl.td = touchOffset;
      tl.ballPosition = ball.body.position;
    }
  }

  void removeTouchLine() {
    isTouchLineActive = false;
    isBulletTiming = false;
    if (tl != null) {
      tl.shouldDestroy = true;
      tl = null;
    }
  }

  int priority() => 2;
}
